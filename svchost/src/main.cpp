/*
 ============================================================================
 Name        : server.cpp
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C++,
 ============================================================================
 */

#include "ServiceHost.h"
#include "jupiter/pool/memory_pool.h"
#include <boost/format/format_fwd.hpp>
#include <boost/format/free_funcs.hpp>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <glib.h>
#include <iostream>
#include <jupiter/jupiter.h>
#include <memory>
#include <signal.h>
#include <string>
#include <sys/time.h>
#include <sys/types.h>

using namespace std;

void initCommandLineHandler() {
	auto monitorQueue = dispatch_queue_create("Monitor Queue", NULL);

	auto source = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, 0, 0, monitorQueue);
	dispatch_source_set_event_handler(source, ^(void) {
		auto count = dispatch_source_get_data(source);
		auto commandBuffer = std::shared_ptr<char>(( char * )Jupiter::MemoryPool::get_instance()->alloc(count * sizeof(char)));
		auto readedCount = read(0, commandBuffer.get(), count);
		*(commandBuffer.get() + readedCount - 1) = '\0';
		auto command = std::string(commandBuffer.get());

		if (( uint )readedCount != count) {
			perror(boost::str(boost::format("I/O stream error, expect count = %1%, as received bytes count = %2%, content = %3%") % count % readedCount % command).c_str());
			return;
		}

		if (count > 2048) {
			perror(boost::str(boost::format("control command error!\nCommand received is: %1%") % command).c_str());
			return;
		}

		std::transform(command.begin(), command.end(), command.begin(), [](unsigned char c) { return std::tolower(c); });
		if (command == "quit" || command == "q") {
			exit(0);
		}
	});

	dispatch_resume(source);
}

int main(int argc, char **argv) {
	sigset(SIGCHLD, SIG_IGN);

	// initCommandLineHandler();

	// dispatch_main();

	gchar *option_service_name = nullptr;
	gchar *option_factory_name = nullptr;
	gchar *option_library_path = nullptr;
	gchar *option_socket_name = nullptr;
	gchar *option_service_type = nullptr;
	GOptionEntry options[] = {
		{ "service_name", 'n', 0, G_OPTION_ARG_STRING, &option_service_name, "Name of service", "" },
		{ "factory_name", 'f', 0, G_OPTION_ARG_STRING, &option_factory_name, "Name of factory function", "" },
		{ "library_path", 'l', 0, G_OPTION_ARG_STRING, &option_library_path, "Path of service's library", "" },
		{ "socket_name", 's', 0, G_OPTION_ARG_STRING, &option_socket_name, "Path of container's socket file", "" },
		{ "service_type", 't', 0, G_OPTION_ARG_STRING, &option_service_type, "Type of service", "" },
		{ nullptr },
	};

	GOptionContext *context;
	GError *err = nullptr;
	GOptionGroup *g_group;
	context = g_option_context_new(nullptr);
	g_group = g_option_group_new("Jupiter svchost native", "Jupiter service host for native service", "Contains a service written in native language (C++)", nullptr, nullptr);
	g_option_context_add_main_entries(context, options, nullptr);
	g_option_context_add_group(context, g_group);
	if (g_option_context_parse(context, &argc, &argv, &err) == FALSE) {
		if (err != nullptr) {
			g_printerr("%s\n", err->message);
			g_error_free(err);
		} else {
			g_printerr("An unknown error occurred\n");
		}

		exit(1);
	}
	g_option_context_free(context);

	if (option_service_name == nullptr || option_library_path == nullptr) {
		g_printerr("No service specified!\n");
		exit(-1);
	}

	auto serviceType = option_service_type == nullptr ? Jupiter::Core::ServiceType::Stateless : strcmp(option_service_type, "stateless") == 0 ? Jupiter::Core::ServiceType::Stateless : Jupiter::Core::ServiceType::Statefull;

	g_free(option_service_type);
	g_free(option_socket_name);

	std::string factoryFunctionName = "createPluginFactory";
	if (option_factory_name != nullptr) {
		factoryFunctionName = option_factory_name;
	}
	g_free(option_factory_name);

	Jupiter::Core::ServiceHost container;
	if (container.init(option_service_name, option_library_path, factoryFunctionName, serviceType)) {
		container.start(option_socket_name, [](bool isSuccessful) {
			if (!isSuccessful) {
				perror("Failed to start service!");
				exit(-1);
			}
		});

		g_free(option_service_name);
		g_free(option_library_path);

		cout << boost::str(boost::format("Service %1% started, waitting for control command...") % option_service_name) << endl;

		initCommandLineHandler();

		dispatch_main();
	} else {
		g_free(option_service_name);
		g_free(option_library_path);

		perror("Failed to init service!");
		return -2;
	}

	return 0;
}
