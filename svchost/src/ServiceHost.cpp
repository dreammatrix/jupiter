/*
 * ServerHostHost.cpp
 *
 *  Created on: Jul 5, 2014
 *      Author: unalone
 */

#include "ServiceHost.h"
#include <arpa/inet.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <cstdlib>
#include <dlfcn.h>
#include <errno.h>
#include <functional>
#include <jupiter/protocols/ServiceHost.pb.h>
#include <map>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <unistd.h>


#include <jupiter/protocols/JupiterApi.pb.h>
namespace Jupiter {
	namespace Core {
		void ServiceHost::start(const std::string &sockName, const Action<bool> &completion) {
			_connection = Network::connectToServer(sockName, [=](bool isConnected) {
				if (isConnected) {
					_connection->setChannelInitializer(std::bind(&ServiceHost::setupClientChannel, this, std::placeholders::_1, std::placeholders::_2));

					auto &&channel = _connection->fork();
					if (channel != nullptr) {
						auto parameter = ProtocolPool<Jupiter::ServiceHost::RegisterServiceParameter>::construct();
						parameter->set_name(_serviceName);

						channel->send(Jupiter::ServiceHost::MessageId_RegisterService, parameter);
					} else {
						_connection->close();
						_connection = nullptr;
					}
				} else {
					_connection = nullptr;
				}

				completion(isConnected);
			});
		}

		bool ServiceHost::init(const std::string &serviceName, const std::string &libName, const std::string &factoryFunctionName, ServiceType serviceType) {
			if (serviceName.empty() || libName.empty()) {
				return false;
			}
			_serviceName = serviceName;
#ifdef ALL_IN_ONE
#else
			_loader = MemPoolAllocater<PluginLoader<Service>>::construct(libName, factoryFunctionName);
			if (!_loader->initialize()) {
				return false;
			}
#endif

			return true;
		}

		void ServiceHost::shutdown(void) {
			_connection->close();
			_connection = nullptr;
#ifndef ALL_IN_ONE
			_loader = nullptr;
#endif

			_serviceInstances.clear();

			exit(0);
		}

		void ServiceHost::acquireService(const std::string &serviceName, const Action<const Network::ChannelRef &> &completion) {
			auto parameter = ProtocolPool<Jupiter::AcquireServiceParameter>::construct();
			parameter->set_servicename(serviceName);

			const auto &serviceChannel = _connection->fork();
			if (serviceChannel != nullptr) {
				serviceChannel->invoke("acquire_service", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &response) {
					if (errorCode == Jupiter::RpcErrorCode_Ok) {
						auto returnValue = dynamic_cast<Jupiter::AcquireServiceReturnValue *>(response.get());
						if (returnValue->errorcode() == Jupiter::AcquireServiceReturnValue::ErrorCode_Ok) {
							completion(serviceChannel);
							return;
						}
					}

					completion(nullptr);
				});
			} else {
				completion(nullptr);
			}
		}

		void ServiceHost::onSetupChannel(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message) {
			auto realParameter = dynamic_cast<Jupiter::ServiceHost::SetupChannelParameter *>(message.get());
#ifdef ALL_IN_ONE
			auto instance = createServiceInstance(_serviceName);
#else
			auto instance = createServiceInstance();
#endif

			if (instance != nullptr) {
				instance->clientConnected(realParameter->address(), channel);
				_serviceInstances.push_back(instance);
			}
		}

#ifdef ALL_IN_ONE
		ServiceRef ServiceHost::createServiceInstance(const std::string &serviceName) {
			void *object = Utilities::dynamic_class_create(serviceName.c_str());
			Service *instance = reinterpret_cast<Service *>(object);

			if (!instance->init(shared_from_this())) {
				return ServiceRef();
			}
			return ServiceRef(instance);
		}
#else

		void ServiceHost::onExit(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message) {
			shutdown();
		}

		void ServiceHost::setupClientChannel(const Network::ConnectionRef &connection, const Network::ChannelRef &channel) {
			const auto configs = {
				Network::createMessageConfig<Jupiter::AcquireServiceReturnValue>("acquire_service"),
				Network::createEmptyMessageConfig(Jupiter::ServiceHost::MessageId::MessageId_Exit, std::bind(&ServiceHost::onExit, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)),
				Network::createMessageConfig<Jupiter::ServiceHost::SetupChannelParameter>(Jupiter::ServiceHost::MessageId::MessageId_SetupChannel, std::bind(&ServiceHost::onSetupChannel, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)),
			};
			channel->setMessageConfig(configs);
		}

		ServiceRef ServiceHost::createServiceInstance(void) {
			if (_serviceType == ServiceType::Statefull) {
				if (_serviceInstance == nullptr) {
					_serviceInstance = _loader->createInstance();
				}

				return _serviceInstance;
			} else {
				auto &&instance = _loader->createInstance();
				if (instance == nullptr) {
					return ServiceRef();
				}

				if (!instance->init(shared_from_this())) {
					return ServiceRef();
				}

				return instance;
			}
		}
#endif
	}  // namespace Core
}  // namespace Jupiter
