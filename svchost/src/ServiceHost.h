/*
 * ServerHost.h
 *
 *  Created on: Jul 5, 2014
 *      Author: unalone
 */

#ifndef ServiceContainer_H_
#define ServiceContainer_H_

#include "jupiter/network/Types.h"
#include <condition_variable>
#include <jupiter/jupiter.h>
#include <mutex>
#include <queue>
#include <sys/socket.h>
#include <thread>
#include <vector>

namespace Jupiter {
	namespace Core {

		enum class ServiceType { Stateless, Statefull };

		class ServiceHost : public IServiceContainer, public std::enable_shared_from_this<ServiceHost> {
		public:
			bool init(const std::string &serviceName, const std::string &libName, const std::string &factoryFunctionName, ServiceType serviceType);

			void start(const std::string &sockName, const Action<bool> &completion);
			void shutdown(void);

			virtual void acquireService(const std::string &serviceName, const Action<const Network::ChannelRef &> &completion) override;

		protected:
			Network::ConnectionRef _connection;
			std::vector<ServiceRef> _serviceInstances;
			std::string _serviceName;

			ServiceType _serviceType;
			ServiceRef _serviceInstance;
			Network::ChannelRef _masterChannel;
#ifdef ALL_IN_ONE
#else
			PluginLoaderRef<Service> _loader;
#endif

			void onSetupChannel(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message);
			void onExit(const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message);
			void setupClientChannel(const Network::ConnectionRef &connection, const Network::ChannelRef &channel);

#ifdef ALL_IN_ONE
			ServiceRef createServiceInstance(const std::string &serviceName);
#else
			ServiceRef createServiceInstance(void);
#endif
		};
	}  // namespace Core
} /* namespace Jupiter */

#endif /* ServiceContainer_H_ */
