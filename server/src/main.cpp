/*
 ============================================================================
 Name        : server.cpp
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C++,
 ============================================================================
 */
#include <config.h>

#include <glib.h>
#include <iostream>
#include <jupiter/jupiter.h>
// #include <worker/WorkerServe/r.h>
#include "Server.h"
#include <jupiter/protocols/JupiterApi.pb.h>
#include <thread>
#include <unistd.h>

using namespace std;
// extern void _dispatch_queue_wakeup_main(void);

struct InitParameter {
	std::shared_ptr<Jupiter::Server::Server> server;
	std::string configFileName;
};

void initCommandLineHandler() {
	cout << boost::str(boost::format("Server started, enter \"quit|q + enter\" to exit.")) << endl;
	auto monitorQueue = dispatch_queue_create("Monitor Queue", NULL);

	auto source = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, 0, 0, monitorQueue);
	dispatch_source_set_event_handler(source, ^(void) {
		auto count = dispatch_source_get_data(source);
		auto commandBuffer = std::shared_ptr<char>(( char * )Jupiter::MemoryPool::get_instance()->alloc(count * sizeof(char)));
		auto readedCount = read(0, commandBuffer.get(), count);
		*(commandBuffer.get() + readedCount - 1) = '\0';
		auto command = std::string(commandBuffer.get());

		if (( uint )readedCount != count) {
			perror(boost::str(boost::format("I/O stream error, expect count = %1%, as received bytes count = %2%, content = %3%") % count % readedCount % command).c_str());
			return;
		}

		if (count > 2048) {
			perror(boost::str(boost::format("control command error!\nCommand received is: %1%") % command).c_str());
			return;
		}

		std::transform(command.begin(), command.end(), command.begin(), [](unsigned char c) { return std::tolower(c); });
		if (command == "quit" || command == "q") {
			exit(0);
		}
	});

	dispatch_resume(source);
}

void init(evutil_socket_t, short, void *);

int main(int argc, char **argv) {
	sigset(SIGCHLD, SIG_IGN);

	gboolean *needRunInBackground = nullptr;
	GOptionEntry options[] = {
		{ "background", 'b', 0, G_OPTION_ARG_NONE, &needRunInBackground, "Run in background", "" },
		{ nullptr },
	};

	GOptionContext *context;
	GError *err = nullptr;
	GOptionGroup *g_group;
	context = g_option_context_new(nullptr);
	g_group = g_option_group_new("Jupiter server", "Jupiter server", "", nullptr, nullptr);
	g_option_context_add_main_entries(context, options, nullptr);
	g_option_context_add_group(context, g_group);
	if (g_option_context_parse(context, &argc, &argv, &err) == FALSE) {
		if (err != nullptr) {
			g_printerr("%s\n", err->message);
			g_error_free(err);
		} else {
			g_printerr("An unknown error occurred\n");
		}

		exit(1);
	}
	g_option_context_free(context);

	if (needRunInBackground != nullptr && *needRunInBackground) {

	} else {
		initCommandLineHandler();
	}

	auto server = Jupiter::MemPoolAllocater<Jupiter::Server::Server>::construct();

	InitParameter parameter = { server, APP_PREFIX + "/etc/jupiter/config.json" };
	struct event *timer_event = event_new(Jupiter::EventLoop::mainLoop()->eventBase(), EV_TIMEOUT, 0, init, &parameter);

	struct timeval time_out_value = { 1, 0 };

	event_add(timer_event, &time_out_value);


	Jupiter::EventLoop::mainLoop()->runInThread();
	dispatch_main();

	return 0;
}

void init(evutil_socket_t, short, void *data) {
	InitParameter *parameter = ( InitParameter * )data;
	// parameter->server->init(parameter->configFileName);
	parameter->server->start(parameter->configFileName);
}
