/*
 * WorkerServer.cpp
 *
 *  Created on: Feb 15, 2015
 *      Author: unalone
 */

#include "./WorkerServer.h"
#include <arpa/inet.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <errno.h>
#include <jupiter/protocols/JupiterApi.pb.h>
#include <jupiter/protocols/ServiceHost.pb.h>
#include <map>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <unistd.h>

namespace Jupiter {
	namespace Server {
		namespace Worker {

			static const std::string SOCKET_FILE_NAME = "/var/tmp/jupiter.worker.socket";

			WorkerServer::WorkerServer() {
			}

			WorkerServer::~WorkerServer() {
			}

			bool WorkerServer::onInit(const boost::property_tree::ptree& document) {
				auto servicesNode = document.get_child_optional("services");
				if (servicesNode.is_initialized()) {
					_serviceHostConnection = Network::connectionForListen(SOCKET_FILE_NAME, [=](const Action<>& completion, const Network::ConnectionRef& incomingConnection) {
						_serviceConnections.push_back(incomingConnection);

						incomingConnection->connectionClosed()->addObserver([this](const Action<>& completion, const Network::ConnectionRef& connection) {
							std::remove_if(_clients.begin(), _clients.end(), [&](const Network::ConnectionRef& client) { return connection.get() == client.get(); });
							completion();
						});

						incomingConnection->fork(0, [this](const Network::ChannelRef& channel) {
							std::vector<Network::MessageConfigPtr> configs = {
								Jupiter::Network::createMessageConfig<Jupiter::AcquireServiceParameter, Jupiter::AcquireServiceReturnValue>("acquire_service", std::bind(&WorkerServer::onAcquireServiceCommand, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
								Jupiter::Network::createMessageConfig<Jupiter::ServiceHost::RegisterServiceParameter>(Jupiter::ServiceHost::MessageId_RegisterService, std::bind(&WorkerServer::onRegisterService, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
								Jupiter::Network::createMessageConfig<Jupiter::ServiceHost::AllocChannelParameter>("alloc_channel"),
							};
							channel->setMessageConfig(configs);
						});

						completion();
					});

					for (auto& node : servicesNode.get()) {
						Utilities::OperationQueue::mainQueue().enqueue([=] {
							auto instance = MemPoolAllocater<ServiceHostInstance>::construct();
							if (instance->initialize(node.second)) {
								_serviceInstances.push_back(instance);
								instance->start(SOCKET_FILE_NAME);
							}
						});
					}
				} else {
					return false;
				}

				return true;
			}

			void WorkerServer::acquireService(const std::string& serviceName, const std::function<void(const Network::ChannelRef&)>& completion) {
				auto it =
					std::find_if(_serviceInstances.begin(), _serviceInstances.end(), [&](const ServiceHostInstanceRef& instance) { return instance->serviceName() == serviceName; });

				if (it == _serviceInstances.end()) {
					//TODO: Try to acquire service from master server
					completion(nullptr);
				} else {
					(*it)->connection()->fork([=](const Network::ChannelRef& serviceChannel) {
						auto parameter = ProtocolPool<Jupiter::ServiceHost::AllocChannelParameter>::construct();
						parameter->set_channelid(serviceChannel->channelId());

						(*it)->connection()->channelOfId(0, [=](const Network::ChannelRef& masterChannel) {
							masterChannel->invoke("alloc_channel", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef& result) {
								if (errorCode == Jupiter::RpcErrorCode_Ok) {
									completion(serviceChannel);
								} else {
									serviceChannel->close([=] { completion(nullptr); });
								}
							});
						});
					});
				}
			}

			void WorkerServer::initializeServiceChannel(const Network::ChannelRef& clientChannel, const Network::ChannelRef& serviceChannel) {
				serviceChannel->setPairedChannel(clientChannel);
				clientChannel->setPairedChannel(serviceChannel);
			}

			void WorkerServer::onShutdown(void) {
				for (auto& instance : _serviceInstances) { instance->stop(); }

				_serviceInstances.clear();
			}

			void WorkerServer::onAcquireServiceCommand(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = dynamic_cast<Jupiter::AcquireServiceParameter*>(parameter.get());

				acquireService(realParameter->servicename(), [=](const Network::ChannelRef& serviceChannel) {
					auto response = ProtocolPool<Jupiter::AcquireServiceReturnValue>::construct();
					auto errorCode = Jupiter::RpcErrorCode_InvalidParameter;

					if (serviceChannel == nullptr) {
						response->set_errorcode(Jupiter::AcquireServiceReturnValue::ErrorCode_ServiceNotAvailiable);
					} else {
						channel->connection()->fork(serviceChannel->channelId(), [=](const Network::ChannelRef& clientChannel) {
							initializeServiceChannel(clientChannel, serviceChannel);

							response->set_channelid(clientChannel->channelId());
							response->set_errorcode(Jupiter::AcquireServiceReturnValue::ErrorCode_Ok);
						});
					}

					completion(errorCode, response);
				});
			}

			void WorkerServer::onRegisterService(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = dynamic_cast<Jupiter::ServiceHost::RegisterServiceParameter*>(parameter.get());

				auto it = std::find_if(_serviceInstances.begin(), _serviceInstances.end(), [&](const ServiceHostInstanceRef& instance) {
					return instance->serviceName() == realParameter->name();
				});
				if (it != _serviceInstances.end()) {
					(*it)->setConnection(channel->connection());
					completion(Jupiter::RpcErrorCode_Ok, nullptr);
				} else {
					completion(Jupiter::RpcErrorCode_InvalidParameter, nullptr);
				}
			}

		} /* namespace Worker */
	}	 /* namespace Server */
} /* namespace Jupiter */
