/*
 * WorkerServer.h
 *
 *  Created on: Feb 15, 2015
 *      Author: unalone
 */

#ifndef SERVER_WORKER_WORKERSERVER_H_
#define SERVER_WORKER_WORKERSERVER_H_

#include "ServiceHostInstance.h"
#include "Server.h"

namespace Jupiter {
	namespace Server {
		namespace Worker {

			class WorkerServer : public Server {
			public:
				WorkerServer();
				virtual ~WorkerServer();

			private:
				std::vector<ServiceHostInstanceRef> _serviceInstances;
				std::vector<Network::ConnectionRef> _serviceConnections;
				Network::ConnectionRef _serviceHostConnection;

				virtual bool onInit(const boost::property_tree::ptree& document) override;
				virtual void onShutdown(void) override;
				virtual void acquireService(const std::string& serviceName, const std::function<void(const Network::ChannelRef&)>& completion) override;
				virtual void initializeServiceChannel(const Network::ChannelRef& clientChannel, const Network::ChannelRef& serviceChannel) override;

				void onAcquireServiceCommand(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion);
				void onRegisterService(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion);
			};

		} /* namespace Worker */
	}	 /* namespace Server */
} /* namespace Jupiter */

#endif /* SERVER_WORKER_WORKERSERVER_H_ */
