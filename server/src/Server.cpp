/*
 * Server.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: unalone
 */

#include "Server.h"
#include "jupiter/pool/memory_pool.h"
#include "jupiter/protocols/Network.pb.h"
#include <arpa/inet.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <errno.h>
#include <functional>
#include <jupiter/protocols/JupiterApi.pb.h>
#include <jupiter/protocols/ServiceHost.pb.h>
#include <map>
#include <net/if.h>
#include <net/if_arp.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <tuple>
#include <unistd.h>
#include <utility>

namespace Jupiter {
	namespace Server {

		Server::Server() {
		}

		Server::~Server() {
		}

		bool Server::start(const std::string& configFileName) {
			boost::property_tree::ptree document;
			if (!Jupiter::ResourceManager::sharedInstance()->loadConfig(configFileName, document)) {
				return false;
			}

			for (const auto& element : document.get_child("ports", boost::property_tree::ptree())) {
				auto&& listeningConnection = Network::connectionForListen(element.second.get("adapter", ""), element.second.get("port", 0), std::bind(&Server::onClientConnected, this, std::placeholders::_1, std::placeholders::_2));

				_listeningConnections.push_back(listeningConnection);
			}

			auto socketFileName = document.get("servicePort", "/tmp/jupiter/com.jupiter.server.container.non-ver.socket");
			_serviceHostConnection = Network::connectionForListen(socketFileName, std::bind(&Server::onServiceConnected, this, std::placeholders::_1, std::placeholders::_2));

			for (const auto& element : document.get_child("services", boost::property_tree::ptree())) {
				auto instance = MemPoolAllocater<ServiceHostInstance>::construct();
				if (instance->initialize(element.second)) {
					_serviceInstances.push_back(instance);
					instance->start(socketFileName);
				}
			}

			return true;
		}

		void Server::shutdown(void) {
			for (const auto& instance : _serviceInstances) { instance->stop(); }

			for (const auto& client : _clients) { client->close(); }

			_clients.clear();

			for (const auto& connection : _listeningConnections) { connection->close(); }
			_listeningConnections.clear();

			Jupiter::EventLoop::mainLoop()->stop();
		}

		void Server::onAuthIdentity(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
			completion(Jupiter::RpcErrorCode::RpcErrorCode_Ok, nullptr);
		}

		void Server::onAcquireService(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
			auto realParameter = dynamic_cast<Jupiter::AcquireServiceParameter*>(parameter.get());

			auto clientConnection = channel->connection();
			auto it = std::find_if(_serviceInstances.begin(), _serviceInstances.end(), [&](const auto& instance) { return instance->serviceName() == realParameter->servicename(); });

			auto response = ProtocolPool<Jupiter::AcquireServiceReturnValue>::construct();
			auto errorCode = Jupiter::RpcErrorCode_InvalidParameter;
			if (it == _serviceInstances.end()) {
				// TODO: Try to acquire service from master server
				response->set_errorcode(Jupiter::AcquireServiceReturnValue::ErrorCode_ServiceNotAvailiable);
			} else {
				auto&& serviceConnection = (*it)->connection();
				auto serviceChannel = serviceConnection->fork();
				if (serviceChannel != nullptr) {
					auto parameter = ProtocolPool<Jupiter::ServiceHost::SetupChannelParameter>::construct();

					auto [ address, port ] = channel->address();
					parameter->set_address(address);
					parameter->set_port(port);
					serviceChannel->send(Jupiter::ServiceHost::MessageId::MessageId_SetupChannel, parameter);

					serviceChannel->setAlternativeAddress(clientConnection->address());
					serviceChannel->closed()->addObserver(std::bind(&Server::onChannelClosed, this, std::placeholders::_1, std::placeholders::_2));

					auto cit = _connectionDatas.find(clientConnection->id());
					cit->second->addTunnel(channel->channelId(), serviceConnection, serviceChannel->channelId());

					cit = _connectionDatas.find(serviceConnection->id());
					cit->second->addTunnel(serviceChannel->channelId(), serviceConnection, channel->channelId());

					response->set_errorcode(Jupiter::AcquireServiceReturnValue::ErrorCode_Ok);
					completion(errorCode, response);
				} else {
					errorCode = Jupiter::RpcErrorCode_Internal;
					response->set_errorcode(Jupiter::AcquireServiceReturnValue::ErrorCode_InternalException);
					completion(errorCode, response);
				}
			}

			completion(errorCode, response);
		}

		void Server::onRegisterService(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
			auto realParameter = dynamic_cast<Jupiter::ServiceHost::RegisterServiceParameter*>(parameter.get());

			auto it = std::find_if(
				_serviceInstances.begin(), _serviceInstances.end(), [&](const ServiceHostInstanceRef& instance) { return instance->serviceName() == realParameter->name(); });
			if (it != _serviceInstances.end()) {
				(*it)->setConnection(channel->connection());
				completion(Jupiter::RpcErrorCode_Ok, nullptr);
			} else {
				completion(Jupiter::RpcErrorCode_InvalidParameter, nullptr);
			}
		}

		void Server::processClientDisconnected(const Action<>& completion, const Network::ConnectionRef& connection) {
			std::remove_if(_clients.begin(), _clients.end(), [&](const Network::ConnectionRef& client) { return connection.get() == client.get(); });
			completion();
		}

		std::tuple<Network::ConnectionRef, int> Server::connectionPreprocesor(const Network::ConnectionRef& connection, int channelId) {
			auto cit = _connectionDatas.find(connection->id());
			return cit->second->tryGetPassThroughData(channelId);
		}

		void Server::onChannelClosed(const Action<>& completion, const Network::ChannelRef& channel) {
			auto cit = _connectionDatas.find(channel->connection()->id());
			cit->second->removeTunnel(channel->channelId());
			completion();
		}

		void Server::onClientConnected(const Action<>& completion, const Network::ConnectionRef& connection) {
			_clients.push_back(connection);
			_connectionDatas.insert(std::make_pair(connection->id(), MemPoolAllocater<ConnectionData>::construct()));

			connection->setPreDispatcher(std::bind(&Server::connectionPreprocesor, this, std::placeholders::_1, std::placeholders::_2));
			connection->setChannelInitializer(std::bind(&Server::setupClientChannel, this, std::placeholders::_1, std::placeholders::_2));
			connection->connectionClosed()->addObserver(std::bind(&Server::processClientDisconnected, this, std::placeholders::_1, std::placeholders::_2));

			completion();
		}

		void Server::onServiceConnected(const Action<>& completion, const Network::ConnectionRef& connection) {
			_serviceConnections.push_back(connection);
			_connectionDatas.insert(std::make_pair(connection->id(), MemPoolAllocater<ConnectionData>::construct()));
			connection->setChannelInitializer(std::bind(&Server::setupServiceChannel, this, std::placeholders::_1, std::placeholders::_2));
			connection->setPreDispatcher(std::bind(&Server::connectionPreprocesor, this, std::placeholders::_1, std::placeholders::_2));

			connection->connectionClosed()->addObserver([this](auto& completion, auto& connection) {
				std::remove_if(_clients.begin(), _clients.end(), [&](auto& client) { return connection.get() == client.get(); });
				completion();
			});

			completion();
		}

		void Server::setupClientChannel(const Network::ConnectionRef& connection, const Network::ChannelRef& channel) {
			auto configs = {
				Jupiter::Network::createMessageConfig<Jupiter::AuthIdentityParameter, Jupiter::AuthIdentityReturnValue>("auth_identity", std::bind(&Server::onAuthIdentity, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
				Jupiter::Network::createMessageConfig<Jupiter::AcquireServiceParameter, Jupiter::AcquireServiceReturnValue>("acquire_service", std::bind(&Server::onAcquireService, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
			};
			channel->setMessageConfig(configs);
			channel->closed()->addObserver(std::bind(&Server::onChannelClosed, this, std::placeholders::_1, std::placeholders::_2));
		}

		void Server::setupServiceChannel(const Network::ConnectionRef& connection, const Network::ChannelRef& channel) {
			auto configs = {
				Jupiter::Network::createMessageConfig<Jupiter::AcquireServiceParameter, Jupiter::AcquireServiceReturnValue>("acquire_service", std::bind(&Server::onAcquireService, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
				Jupiter::Network::createMessageConfig<Jupiter::ServiceHost::RegisterServiceParameter>(Jupiter::ServiceHost::MessageId_RegisterService, std::bind(&Server::onRegisterService, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
			};
			channel->setMessageConfig(configs);
			channel->closed()->addObserver(std::bind(&Server::onChannelClosed, this, std::placeholders::_1, std::placeholders::_2));
		}

	} /* namespace Server */
} /* namespace Jupiter */
