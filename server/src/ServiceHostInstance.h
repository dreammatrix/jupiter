/*
 * ServiceHostInstance.h
 *
 *  Created on: Feb 19, 2015
 *      Author: unalone
 */

#ifndef SERVER_WORKER_SERVICEHOSTINSTANCE_H_
#define SERVER_WORKER_SERVICEHOSTINSTANCE_H_

#include <jupiter/jupiter.h>
#include <unistd.h>

namespace Jupiter {
	namespace Server {

		class ServiceHostInstance {
		public:
			~ServiceHostInstance();

			const Network::ConnectionRef& connection() const;
			void setConnection(const Network::ConnectionRef& connection);

			pid_t pid() const;

			const std::string& execEntryPath() const;

			const std::string& serviceName() const;

			bool initialize(const boost::property_tree::ptree& serviceNode);
			void start(const std::string& socketFileName);
			void stop();

		private:
			Utilities::OperationQueue _queue;
			pid_t _pid = -1;
			std::string _name;
			std::string _execEntryPath;
			Network::ConnectionRef _connection;
			intptr_t _connectionClosedToken = 0;
			std::string _socketFileName;

			void onConnectionClosed(const Action<>& completion, const Network::ConnectionRef& connecton);
			void connectToService();
		};

		typedef std::shared_ptr<ServiceHostInstance> ServiceHostInstanceRef;

	} /* namespace Server */
} /* namespace Jupiter */

#endif /* SERVER_WORKER_SERVICEHOSTINSTANCE_H_ */
