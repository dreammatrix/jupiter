/*
 * GateServer.cpp
 *
 *  Created on: Feb 6, 2015
 *      Author: unalone
 */

#include <arpa/inet.h>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <errno.h>
#include <map>
#include <net/if_arp.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/types.h>
#include <unistd.h>
#include <jupiter/protocols/JupiterApi.pb.h>
#include "GateServer.h"

namespace Jupiter {
	namespace Server {
		namespace Gate {

			GateServer::GateServer () {
			}

			GateServer::~GateServer () {
			}

			bool GateServer::onInit (const boost::property_tree::ptree& document) {
				for (auto &element : document.get_child("workers")) {
					_workerServers.insert(std::make_pair(element.first, std::make_pair(element.second.get("address", ""), element.second.get("port", (short) 0))));
				}

				return true;
			}

			void GateServer::acquireService (const std::string& serviceName, const std::function<void (const Network::ChannelRef&)>& completion) {
				auto it = _workerServerConnections.find(serviceName);
				auto errorCode = Jupiter::RpcErrorCode_InvalidParameter;
				if (it == _workerServerConnections.end()) {
					completion(nullptr);
				} else {
					it->second->channelOfId(0, [=] (const Network::ChannelRef &serverChannel) {
						auto parameter = ProtocolPool<Jupiter::AcquireServiceParameter>::construct();
						parameter->set_servicename(serviceName);

						serverChannel->invoke("acquire_service", parameter, [=] (Jupiter::RpcErrorCode errorCode, const Network::MessageRef &result) {
									auto serverResponse = dynamic_cast<Jupiter::AcquireServiceReturnValue*>(result.get());
									if (serverResponse->errorcode() == Jupiter::AcquireServiceReturnValue::ErrorCode_Ok) {
										it->second->fork(serverResponse->channelid(), completion);
									} else {
										completion(nullptr);
									}
								});
					});
				}
			}

			void GateServer::initializeServiceChannel (const Network::ChannelRef& clientChannel, const Network::ChannelRef& serviceChannel) {
				serviceChannel->setPairedChannel(clientChannel);
				clientChannel->setPairedChannel(serviceChannel);
			}


			void GateServer::onShutdown (void) {
				for (auto &pair : _workerServerConnections) {
					pair.second->close();
				}

				_workerServerConnections.clear();
			}

		} /* namespace Gate */
	} /* namespace Server */
} /* namespace Jupiter */
