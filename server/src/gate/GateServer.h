/*
 * GateServer.h
 *
 *  Created on: Feb 6, 2015
 *      Author: unalone
 */

#ifndef SERVER_GATE_GATESERVER_H_
#define SERVER_GATE_GATESERVER_H_

#include <condition_variable>
#include <jupiter/jupiter.h>
#include "Server.h"
#include <mutex>
#include <queue>
#include <sys/socket.h>
#include <thread>
#include <vector>
#include <map>

namespace Jupiter {
	namespace Server {
		namespace Gate {

			class GateServer: public Server {
			public:
				GateServer ();
				virtual ~GateServer ();

			protected:
				std::map<std::string, std::pair<std::string, uint16_t>> _workerServers;
				std::map<std::string, Network::ConnectionRef> _workerServerConnections;

				virtual bool onInit (const boost::property_tree::ptree &document) override;
				virtual void onShutdown (void) override;
				virtual void acquireService (const std::string &serviceName, const std::function<void (const Network::ChannelRef&)> &completion) override;
				virtual void initializeServiceChannel (const Network::ChannelRef &clientChannel, const Network::ChannelRef &serviceChannel) override;
			};

		} /* namespace Gate */
	} /* namespace Server */
} /* namespace Jupiter */

#endif /* SERVER_GATE_GATESERVER_H_ */
