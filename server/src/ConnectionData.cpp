#include "ConnectionData.h"
#include <exception>
#include <tuple>
#include <utility>

namespace Jupiter {
	namespace Server {
		class DuplicateTunnelException: public std::exception {
		};

		void ConnectionData::addTunnel(int inChannel, const Jupiter::Network::ConnectionRef &connection, int outChannel) {
			if (_tunnels.find(inChannel) != _tunnels.end()) {
				throw DuplicateTunnelException();
			}

			_tunnels.insert(std::make_pair(inChannel, std::make_tuple(connection, outChannel)));
		}

		void ConnectionData::removeTunnel(int inChannel) {
			_tunnels.erase(inChannel);
		}

		std::tuple<Jupiter::Network::ConnectionRef, int> ConnectionData::tryGetPassThroughData(int channelId) {
			auto it = _tunnels.find(channelId);
			if (it != _tunnels.end()) {
				return it->second;
			}

			return std::make_tuple(nullptr, 0);
		}
	}
}