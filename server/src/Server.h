/*
 * Server.h
 *
 *  Created on: Feb 19, 2015
 *      Author: unalone
 */

#ifndef SERVER_GATE_SERVER_H_
#define SERVER_GATE_SERVER_H_

#include "ConnectionData.h"
#include "ServiceHostInstance.h"
#include "jupiter/network/Types.h"
#include <boost/property_tree/ptree.hpp>
#include <jupiter/jupiter.h>
#include <map>

namespace Jupiter {
	namespace Server {

		class Server {
		public:
			Server();
			virtual ~Server();

			bool start(const std::string &configFileName);
			void shutdown(void);

		private:
			std::vector<Network::ConnectionRef> _listeningConnections;

			void onAuthIdentity(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);
			void onAcquireService(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);
			void onRegisterService(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);

			void processClientDisconnected(const Action<> &completion, const Network::ConnectionRef &connection);

		protected:
			std::vector<Network::ConnectionRef> _clients;
			std::vector<ServiceHostInstanceRef> _serviceInstances;
			std::vector<Network::ConnectionRef> _serviceConnections;
			Network::ConnectionRef _serviceHostConnection;

			std::map<uint64_t, std::shared_ptr<ConnectionData>> _connectionDatas;

			std::tuple<Network::ConnectionRef, int> connectionPreprocesor(const Network::ConnectionRef &connection, int channelId);

			void onChannelClosed(const Action<> &completion, const Network::ChannelRef &channel);
			void onClientConnected(const Action<> &completion, const Network::ConnectionRef &connection);
			void onServiceConnected(const Action<> &completion, const Network::ConnectionRef &connection);

			void setupClientChannel(const Network::ConnectionRef &connection, const Network::ChannelRef &channel);
			void setupServiceChannel(const Network::ConnectionRef &connection, const Network::ChannelRef &channel);
		};

	} /* namespace Server */
} /* namespace Jupiter */

#endif /* SERVER_GATE_SERVER_H_ */
