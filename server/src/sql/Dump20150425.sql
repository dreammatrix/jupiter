CREATE DATABASE  IF NOT EXISTS `com.bigbang.games` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `com.bigbang.games`;
-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: 172.16.19.128    Database: com.bigbang.games
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bigbang_slot_character`
--

DROP TABLE IF EXISTS `bigbang_slot_character`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bigbang_slot_character` (
  `id` bigint(20) unsigned NOT NULL,
  `character_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`,`character_id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bigbang_slot_character`
--

LOCK TABLES `bigbang_slot_character` WRITE;
/*!40000 ALTER TABLE `bigbang_slot_character` DISABLE KEYS */;
/*!40000 ALTER TABLE `bigbang_slot_character` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `character`
--

DROP TABLE IF EXISTS `character`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `character` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `app_id` varchar(128) NOT NULL,
  `vip_level` int(10) unsigned DEFAULT '0',
  PRIMARY KEY (`id`,`user_id`,`app_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `id_index` (`user_id`,`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `character`
--

LOCK TABLES `character` WRITE;
/*!40000 ALTER TABLE `character` DISABLE KEYS */;
/*!40000 ALTER TABLE `character` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_center_user`
--

DROP TABLE IF EXISTS `game_center_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_center_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `gamer_tag` varchar(128) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`gamer_tag`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `gamer_tag_UNIQUE` (`gamer_tag`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_center_user`
--

LOCK TABLES `game_center_user` WRITE;
/*!40000 ALTER TABLE `game_center_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_center_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest_user`
--

DROP TABLE IF EXISTS `guest_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` varchar(128) NOT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`device_id`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `device_id_UNIQUE` (`device_id`),
  UNIQUE KEY `userId_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest_user`
--

LOCK TABLES `guest_user` WRITE;
/*!40000 ALTER TABLE `guest_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `guest_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_user`
--

DROP TABLE IF EXISTS `oauth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_user` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `tag` varchar(128) NOT NULL,
  `token` varchar(128) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`tag`,`user_id`,`type`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `tag_UNIQUE` (`tag`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`),
  KEY `service_type_index` (`type`,`tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_user`
--

LOCK TABLES `oauth_user` WRITE;
/*!40000 ALTER TABLE `oauth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_lock_ journal`
--

DROP TABLE IF EXISTS `resource_lock_ journal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_lock_ journal` (
  `id` bigint(20) unsigned NOT NULL,
  `resourceId` bigint(20) unsigned DEFAULT NULL,
  `service` varchar(255) NOT NULL,
  `service_address` varchar(127) NOT NULL,
  `count` bigint(20) DEFAULT NULL,
  `from` datetime DEFAULT NULL,
  PRIMARY KEY (`service`,`service_address`,`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `id_from` (`from`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_lock_ journal`
--

LOCK TABLES `resource_lock_ journal` WRITE;
/*!40000 ALTER TABLE `resource_lock_ journal` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_lock_ journal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resources`
--

DROP TABLE IF EXISTS `resources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resources` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `resource_type` varchar(128) NOT NULL,
  `count` bigint(20) DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL,
  `character_id` bigint(20) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`,`resource_type`,`user_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `union_key_UNIQUE` (`resource_type`(10),`user_id`,`character_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resources`
--

LOCK TABLES `resources` WRITE;
/*!40000 ALTER TABLE `resources` DISABLE KEYS */;
/*!40000 ALTER TABLE `resources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `ref_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '真正用户数据表的id',
  `nick_name` varchar(128) NOT NULL,
  `gender` tinyint(2) unsigned DEFAULT '0',
  `avatar_url` varchar(256) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`type`,`ref_id`),
  UNIQUE KEY `userid_UNIQUE` (`user_id`),
  UNIQUE KEY `nick_name_UNIQUE` (`nick_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,0,0,'',1,NULL,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'com.bigbang.games'
--
/*!50003 DROP PROCEDURE IF EXISTS `create_resource` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `create_resource`(in userId bigint(20), in characterId bigint(20), in resourceType varchar(128),
 in initialCount bigint(20), out isSuccessful boolean)
BEGIN

select id from resources where user_id=userId and character_id=characterId and resource_type=resourceType;
if found_rows()=0 then
	insert into resources (resource_type, count, user_id, character_id) values (resourceType, initialCount, userId, characterId);
	set isSuccessful=true;
else
	set isSuccessful=false;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `process_login` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `process_login`(in ref_id varchar(128), in token varchar(128), in account_type int4)
BEGIN

declare _userId bigint(20);

if account_type=0 then
	select user_id into _userId from guest_user where device_id=ref_id;
elseif account_type=1 then
	select user_id into _userId from game_center_user where gamer_tag=ref_id;
elseif account_type=1 then
	select user_id into _userId from oauth_user where tag=ref_id and `token`=token;
end if;

if FOUND_ROWS()<>0 then
	START transaction;
	select user_id, gender, nick_name, avatar_url, UNIX_TIMESTAMP(create_time) as create_time_value, unix_timestamp(last_login_time) as last_login_time_value
		from users where user_id=_userId for update;
    if found_rows()<>0 then
		update users set last_login_time=now() where user_id=_userId;
        commit;
	else
		rollback;
    end if;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `process_register` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `process_register`(in ref_id varchar(128), in token varchar(128), in account_type int4, in service_type int4, in nickName varchar(128))
BEGIN

declare _userId bigint(20);

if account_type=0 then
	select user_id into _userId from guest_user where device_id=ref_id;
elseif account_type=1 then
	select user_id into _userId from game_center_user where gamer_tag=ref_id;
elseif account_type=1 then
	select user_id into _userId from oauth_user where tag=ref_id and `token`=token;
end if;

if found_rows()=0 then
	start transaction;
	if account_type=0 then
		insert into guest_user (device_id) values (ref_id);
	elseif account_type=1 then
        insert into game_center_user (gamer_tag) values (ref_id);
	elseif account_type=1 then
        insert into oauth_user (tag, `token`, type) values (ref_id, token, service_type);
	end if;
    
    select LAST_INSERT_ID() into _userId;
    
    insert into users (type, ref_id, nick_name, create_time, last_login_time) values (account_type, nickName, now(), now());
    select LAST_INSERT_ID() into _userId;
    
    if account_type=0 then
		update guest_user set user_id=_userId;
	elseif account_type=1 then
        update game_center_user set user_id=_userId;
	elseif account_type=1 then
        update oauth_user set user_id=_userId;
	end if;
    
    select _userId as user_id, 0 as gender, nickName as nick_name, "" as avatar_url, UNIX_TIMESTAMP(now()) as create_time_value, unix_timestamp(now()) as last_login_time_value;
    
    commit;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_user_data` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`%` PROCEDURE `update_user_data`(in userId bigint(20), in gender int4, in nickName varchar(128), in avatarUrl varchar(256), out isSuccessful boolean)
BEGIN

declare _userId bigint(20);

select user_id into _userId from users where user_id=userId for update;

if found_rows()<>0 then
	start transaction;
    
    update users set `gender`=gender, nick_name=nickName, avatar_url=avatarUrl where user_id=userId;
    
    commit;

	set isSuccessful = true;
else
	set isSuccessful = false;
end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-25  0:49:33
