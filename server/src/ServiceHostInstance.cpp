/*
 * ServiceHostInstance.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: unalone
 */

#include "ServiceHostInstance.h"
#include <jupiter/protocols/ServiceHost.pb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

namespace Jupiter {
	namespace Server {

		ServiceHostInstance::~ServiceHostInstance() {
			stop();
		}

		bool ServiceHostInstance::initialize(const boost::property_tree::ptree& serviceNode) {
			_name = serviceNode.get("name", "");
			if (_name.empty()) {
				return false;
			}

			_execEntryPath = serviceNode.get("path", "");
			if (_execEntryPath.empty()) {
				return false;
			}

			return true;
		}

		void ServiceHostInstance::start(const std::string& socketFileName) {
			if (_pid != -1) {
				return;
			}

			_socketFileName = socketFileName;

			auto pid = fork();
			_queue.call([=]() {
				if (pid == 0) {
					execl(_execEntryPath.c_str(), "-s", socketFileName.c_str(), nullptr);
				} else {
					_pid = pid;
				}
			});
		}

		void ServiceHostInstance::stop() {
			if (_pid == -1) {
				return;
			}

			_connection->connectionClosed()->removeObserver(_connectionClosedToken);
			const auto& channel = _connection->channelOfId(0);
			if (channel != nullptr) {
				_queue.call([=] {
					_pid = -1;

					channel->send(Jupiter::ServiceHost::MessageId_Exit);

					int status = 0;
					waitpid(_pid, &status, 0);
				});
			}
		}

		const Network::ConnectionRef& ServiceHostInstance::connection() const {
			return _connection;
		}

		void ServiceHostInstance::setConnection(const Network::ConnectionRef& connection) {
			_queue.call([=] {
				_connection = connection;
				_connectionClosedToken = _connection->connectionClosed()->addObserver(std::bind(&ServiceHostInstance::onConnectionClosed, this, std::placeholders::_1, std::placeholders::_2));
			});
		}

		pid_t ServiceHostInstance::pid() const {
			return _pid;
		}

		const std::string& ServiceHostInstance::execEntryPath() const {
			return _execEntryPath;
		}

		const std::string& ServiceHostInstance::serviceName() const {
			return _name;
		}

		void ServiceHostInstance::onConnectionClosed(const Action<>& completion, const Network::ConnectionRef& connecton) {
			perror(boost::str(boost::format("Service %1% is down, trying to restart!") % _name).c_str());
			_queue.call([=] {
				_connection = nullptr;
				_pid = -1;
			});

			start(_socketFileName);
			completion();
		}

	} /* namespace Server */
} /* namespace Jupiter */
