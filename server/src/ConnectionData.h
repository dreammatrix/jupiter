#include "jupiter/network/Types.h"
#include <map>
#include <tuple>

namespace Jupiter {
	namespace Server {
		class ConnectionData {
		public:
			void addTunnel(int inChannel, const Jupiter::Network::ConnectionRef &connection, int outChannel);
			void removeTunnel(int inChannel);

			std::tuple<Jupiter::Network::ConnectionRef, int> tryGetPassThroughData(int channelId);

		private:
			std::map<int, std::tuple<Jupiter::Network::ConnectionRef, int>> _tunnels;
		};
	}
}
