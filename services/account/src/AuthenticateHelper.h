/*
 * AuthenticateHelper.h
 *
 *  Created on: Apr 8, 2015
 *      Author: unalone
 */

#ifndef SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATEHELPER_H_
#define SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATEHELPER_H_

#include <Cert.pb.h>
#include <jupiter/jupiter.h>
#include <jupiter/protocols/services/account/AuthenticateService.pb.h>

namespace Jupiter {
	namespace Services {
		namespace Account {

			const std::string &&generateToken(const std::string &sessionId, const std::string &appId, uint64_t userId, const std::string &deviceId);

			enum class TokenAuthResultType { Valid, Timeout, Invalid };
			std::tuple<TokenAuthResultType, std::shared_ptr<Jupiter::AuthenticateService::CertData>> isValidToken(const std::string &tokenData, const std::string &deviceId);

		} /* namespace Account */
	}	  /* namespace Services */
} /* namespace Jupiter */

#endif /* SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATEHELPER_H_ */
