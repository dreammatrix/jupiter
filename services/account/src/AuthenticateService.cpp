/*
 * AuthenticateService.cpp
 *
 *  Created on: Apr 7, 2015
 *      Author: unalone
 */

#include "AuthenticateService.h"
#include "AuthenticateHelper.h"
#include "jupiter/pool/ProtocolPool.h"
#include "jupiter/protocols/Network.pb.h"
#include <jupiter/protocols/services/account/AuthenticateService.pb.h>
#include <memory>
#include <tuple>

namespace Jupiter {
	namespace Services {
		namespace Authenticate {

			void AuthenticateService::clientConnected(const std::string& address, const Jupiter::Network::ChannelRef& channel) {
				Service::clientConnected(address, channel);

				const auto configs = {
					Jupiter::Network::createMessageConfig<Jupiter::AuthenticateService::AuthenticateCommandParameter>("authenticate", std::bind(&AuthenticateService::onAuthenticate, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
					Jupiter::Network::createMessageConfig<Jupiter::AuthenticateService::RequestCertParameter>("request_cert", std::bind(&AuthenticateService::onRequestCert, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)),
				};
				channel->setMessageConfig(configs);
			}

			void AuthenticateService::onAuthenticate(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = std::dynamic_pointer_cast<Jupiter::AuthenticateService::AuthenticateCommandParameter>(parameter);
				auto result = ProtocolPool<Jupiter::AuthenticateService::AuthenticateCommandReturnValue>::construct();

				auto [ checkResult, token ] = Account::isValidToken(realParameter->cert(), realParameter->deviceid());
				if (checkResult == Account::TokenAuthResultType::Invalid) {
					result->set_errorcode(::Jupiter::AuthenticateService::ErrorCode::ErrorCode_InvalidCert);
				} else if (checkResult == Account::TokenAuthResultType::Timeout) {
					result->set_errorcode(::Jupiter::AuthenticateService::ErrorCode::ErrorCode_CertNotValidSinceTimeOut);
				} else {
					result->set_sessionid(token->sessionid());
				}

				completion(RpcErrorCode::RpcErrorCode_Ok, result);
			}

			void AuthenticateService::onRequestCert(const Network::ChannelRef& channel, const std::string& methodName, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = std::dynamic_pointer_cast<Jupiter::AuthenticateService::RequestCertParameter>(parameter);
				auto result = ProtocolPool<Jupiter::AuthenticateService::RequestCertReturnValue>::construct();

				auto token = Account::generateToken(realParameter->sessionid(), realParameter->appid(), realParameter->userid(), realParameter->deviceid());
				result->set_cert(token);

				completion(RpcErrorCode::RpcErrorCode_Ok, result);
			}
		} /* namespace Authenticate */
	}	  /* namespace Services */
} /* namespace Jupiter */
