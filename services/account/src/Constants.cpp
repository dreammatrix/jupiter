/*
 * Constants.cpp
 *
 *  Created on: Mar 6, 2015
 *      Author: unalone
 */

#include "Constants.h"

namespace Jupiter {
	namespace Services {
		namespace Account {

			const std::string CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL = "@com.bigbang.poker.services.account.channelProperty.sessionServiceChannel@";
			const std::string CHANNEL_PROPERTY_USER_ID = "@com.bigbang.poker.services.account.channelProperty.userId@";
			const std::string CHANNEL_PROPERTY_GAME_DATA_PROVIDER_CHANNEL = "@com.bigbang.poker.services.account.channelProperty.dataProviderChannel@";

		} /* namespace Account */
	} /* namespace Services */
} /* namespace Jupiter */
