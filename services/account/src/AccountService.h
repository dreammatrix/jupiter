/*
 * AccountService.h
 *
 *  Created on: Feb 26, 2015
 *      Author: unalone
 */

#ifndef SERVICES_ACCOUNT_SERVICE_ACCOUNTSERVICE_H_
#define SERVICES_ACCOUNT_SERVICE_ACCOUNTSERVICE_H_

#include <jupiter/jupiter.h>
#include <jupiter/protocols/services/account/AccountService.pb.h>

namespace Jupiter {
	namespace Services {
		namespace Account {

			class AccountService: public Service {
			public:
				virtual bool init (const ServiceContainerRef &container) override;
				virtual void clientConnected(const std::string &address, const Network::ChannelRef &channel) override;

				ImplementDynamicClass(AccountService);
			private:
				DbConnectionPoolRef _dbPool;
				std::string _dbName;
				std::map<std::string, std::string> _playerDataProviderServices;
				std::map<uint64_t, Network::ChannelRef> _userChannels;

				virtual void onClientDisconnected (const Action<> &completion, const Network::ChannelRef &channel) override;

				void onLogin (const Network::ChannelRef &channel, uint32_t methodId, const Network::MessageRef &parameter,
						const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);
				void onUpdatePlayerData (const Network::ChannelRef &channel, uint32_t methodId, const Network::MessageRef &parameter,
						const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);

				void onPlayerGameDataUpdatedMessage (const Network::ChannelRef &channel, const Network::NetworkPacketRef &packet, const Network::MessageRef &message);

				void tryLogin(const Network::MessageRef& parameter, const Action<const std::shared_ptr<Protocols::AccountService::UserInfo>&, Protocols::AccountService::ErrorCode> &completion);

				void initializePlayerGameDataProviderChannel (const Network::ChannelRef &clientChannel, const Network::ChannelRef &serviceChannel);
			};

			RegisterDynamicClassWithName("com.jupiter.services.account", AccountService);

		} /* namespace Account */
	} /* namespace Services */
} /* namespace Jupiter */

#endif /* SERVICES_ACCOUNT_SERVICE_ACCOUNTSERVICE_H_ */
