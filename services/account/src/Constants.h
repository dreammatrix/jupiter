/*
 * Constants.h
 *
 *  Created on: Mar 6, 2015
 *      Author: unalone
 */

#ifndef SERVICES_COM_BIGBANG_POKER_ACCOUNT_SERVICE_CONSTANTS_H_
#define SERVICES_COM_BIGBANG_POKER_ACCOUNT_SERVICE_CONSTANTS_H_

#include <string>

namespace Jupiter {
	namespace Services {
		namespace Account {

			extern const std::string CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL;
			extern const std::string CHANNEL_PROPERTY_USER_ID;
			extern const std::string CHANNEL_PROPERTY_GAME_DATA_PROVIDER_CHANNEL;

		} /* namespace Account */
	} /* namespace Services */
} /* namespace Jupiter */

#endif /* SERVICES_COM_BIGBANG_POKER_ACCOUNT_SERVICE_CONSTANTS_H_ */
