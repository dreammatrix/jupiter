/*
 * AuthenticateHelper.cpp
 *
 *  Created on: Apr 8, 2015
 *      Author: unalone
 */

#include "AuthenticateHelper.h"
#include "jupiter/utilities/Crypto.h"
#include <event2/util.h>
#include <jupiter/utilities/Crypto.h>
#include <tuple>

namespace Jupiter {
	namespace Services {
		namespace Account {
			static const char ENCRYPT_KEY[] = "af83nf*%^#)$^*(7202-0j8df23+)@^3";

			const std::string&& generateToken(const std::string& sessionId, const std::string& appId, uint64_t userId, const std::string& deviceId) {
				auto token = ProtocolPool<Jupiter::AuthenticateService::CertData>::construct();
				token->set_sessionid(sessionId);
				token->set_appid(appId);
				token->set_userid(userId);
				token->set_deviceid(deviceId);

				timeval now;
				evutil_gettimeofday(&now, nullptr);
				token->set_time(now.tv_sec);
				token->set_validtime(now.tv_sec + (60 * 30));
				token->set_signature(Jupiter::Utilities::Crypto::sha256Hash(deviceId + sessionId));

				std::string buffer;
				token->SerializeToString(&buffer);

				return Jupiter::Utilities::Crypto::aesCrypto(buffer, ENCRYPT_KEY);
			}

			std::tuple<TokenAuthResultType, std::shared_ptr<Jupiter::AuthenticateService::CertData>> isValidToken(const std::string& tokenData, const std::string& deviceId) {
				auto token = ProtocolPool<Jupiter::AuthenticateService::CertData>::construct();
				auto result = TokenAuthResultType::Valid;
				try {
					auto&& buffer = Jupiter::Utilities::Crypto::aesDecrypto(tokenData, ENCRYPT_KEY);
					token->ParseFromString(buffer);

					auto signature = Jupiter::Utilities::Crypto::sha256Hash(deviceId + token->sessionid());
					if (signature != token->signature()) {
						result = TokenAuthResultType::Invalid;
						token.reset();
					}

					timeval now;
					evutil_gettimeofday(&now, nullptr);
					if (token->validtime() < now.tv_sec) {
						result = TokenAuthResultType::Timeout;
						token.reset();
					}
				} catch (...) { return std::make_tuple(TokenAuthResultType::Valid, std::shared_ptr<Jupiter::AuthenticateService::CertData>()); }

				return std::make_tuple(result, token);
			}

		} /* namespace Account */
	}	  /* namespace Services */
} /* namespace Jupiter */
