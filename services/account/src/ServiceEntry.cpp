#include <openssl/evp.h>
#include "AuthenticateService.h"

bool initializeAccountService() {
	return true;
}

void accountServiceWillBeUnloaded() {
	EVP_cleanup();
}

void* createAccountService(void) {
	return new Jupiter::Services::Authenticate::AuthenticateService();
}

bool initializeAuthenticateService() {
	return true;
}

void authenticateServiceWillBeUnloaded() {
	EVP_cleanup();
}

void* createAuthenticateService(void) {
	return new Jupiter::Services::Authenticate::AuthenticateService();
}
