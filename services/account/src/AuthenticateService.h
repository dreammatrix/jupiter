/*
 * AuthenticateService.h
 *
 *  Created on: Apr 7, 2015
 *      Author: unalone
 */

#ifndef SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATESERVICE_H_
#define SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATESERVICE_H_

#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Services {
		namespace Authenticate {
			class AuthenticateService : public Service {
			public:
				AuthenticateService() = default;

				virtual void clientConnected(const std::string &address, const Jupiter::Network::ChannelRef &channel) override;

				ImplementDynamicClass(AuthenticateService);

			private:
				void onAuthenticate(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);
				void onRequestCert(const Network::ChannelRef &channel, const std::string &methodName, const Network::MessageRef &parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef &> &completion);
			};

			RegisterDynamicClassWithName("com.jupiter.services.authenticate", AuthenticateService);

		} /* namespace Authenticate */
	}	  /* namespace Services */
} /* namespace Jupiter */

#endif /* SERVICES_JUPITER_ACCOUNT_SERVICE_AUTHENTICATESERVICE_H_ */
