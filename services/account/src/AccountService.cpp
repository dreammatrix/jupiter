/*
 * AccountService.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: unalone
 */

#include "AccountService.h"
#include "AuthenticateHelper.h"
#include "Constants.h"
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <event2/util.h>
#include <jupiter/protocols/ResourceService.pb.h>
#include <jupiter/protocols/SessionApi.pb.h>
#include <jupiter/protocols/UserDataProvider.pb.h>
#include <jupiter/protocols/services/account/AccountService.pb.h>
#include <sys/time.h>

namespace Jupiter {
	namespace Services {
		namespace Account {

			bool AccountService::init(const ServiceContainerRef& container) {
				if (!Service::init(container)) {
					return false;
				}

				boost::property_tree::ptree config;
				if (!ResourceManager::sharedInstance()->loadConfig("service/com.jupiter.services.account.json", config) || config.empty()) {
					return false;
				}

				auto serverAddress = config.get("db.address", "127.0.0.1:3306");
				auto userName = config.get("db.user_name", "test");
				auto password = config.get("db.password", "test");
				_dbName = config.get("db.password", "jupiter");

				_dbPool = MemPoolAllocater<DbConnectionPool>::construct();
				if (!_dbPool->init(serverAddress, userName, password, 50)) {
					return false;
				}

				auto pluginNode = config.get_child_optional("providers");
				if (!pluginNode.is_initialized()) {
					return false;
				}

				for (auto& element : pluginNode.get()) { _playerDataProviderServices.insert(std::make_pair(element.first, element.second.data())); }

				return true;
			}

			void AccountService::clientConnected(const std::string& address, const Network::ChannelRef& channel) {
				Service::clientConnected(channel);

				channel->setMethodParameterTranslateHandler([](uint32_t methodId) {
					static std::map<uint32_t, std::function<Network::MessageRef()>> table = {
						{ Jupiter::AccountService::MessageId_Login, [] { return ProtocolPool<Jupiter::AccountService::LoginCommandParameter>::construct(); } },
						{ Jupiter::AccountService::MessageId_UpdatePlayerData, [] { return ProtocolPool<Jupiter::AccountService::UpdatePlayerDataParameter>::construct(); } }
					};

					auto it = table.find(methodId);
					if (it == table.end()) {
						return Network::MessageRef();
					}

					return it->second();
				});

				static std::map<uint32_t, Network::RpcMethodType> rpcTable = {
					{ Jupiter::AccountService::MessageId_Login, std::bind(&AccountService::onLogin, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4) },
					{ Jupiter::AccountService::MessageId_UpdatePlayerData, std::bind(&AccountService::onUpdatePlayerData, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4) },
				};
				channel->registerRpcMethods(rpcTable);

				//				_container->acquireService("com.jupiter.services.resource", 0, [=] (const Network::ChannelRef &serviceChannel)
				//{
				//					channel->setProperty(CHANNEL_PROPERTY_RESOURCE_SERVICE_CHANNEL, serviceChannel);
				//
				//					serviceChannel->setMethodResponseTranslateHandler([] (const uint32_t &methodName) {
				//								static std::map<uint32_t, std::function<Network::MessageRef()>> table = {
				//									{	"bind_user_to_channel", [] {return
				// ProtocolPool<Jupiter::ResourceService::BindUserToChannelReturnValue>::construct();}},
				//									{	"query_free_command", [] {return
				// ProtocolPool<Jupiter::ResourceService::QueryFreeCountCommandReturnValue>::construct();}},
				//									{	"query_detial_usage", [] {return
				// ProtocolPool<Jupiter::ResourceService::QueryDetialUsageCommandReturnValue>::construct();}},
				//									{	"lock", [] {return
				// ProtocolPool<Jupiter::ResourceService::LockUnlockCommandReturnValue>::construct();}},
				//									{	"unlock", [] {return
				// ProtocolPool<Jupiter::ResourceService::LockUnlockCommandReturnValue>::construct();}}
				//								};
				//
				//								auto it = table.find(methodName);
				//								if (it == table.end()) {
				//									return Network::MessageRef();
				//								}
				//
				//								return it->second();
				//							});
				//				});

				// recreateServiceChannel("com.jupiter.services.session", 0, channel,
				// 		[=](const Network::ChannelRef& serviceChannel) {
				// 			channel->setProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL, serviceChannel);

				// 			serviceChannel->setMethodResponseTranslateHandler([](uint32_t methodId) {
				// 						static std::map<uint32_t, std::function<Network::MessageRef()>> table = {
				// 							{	"alloc_session", [] {return ProtocolPool<Jupiter::SessionApi::AllocSessionReturnValue>::construct();}},
				// 							{	"bind_session_to_channel",
				// 								[] {return ProtocolPool<Jupiter::SessionApi::BindSessionToChannelReturnValue>::construct();}},
				// 							{	"get_data", [] {return ProtocolPool<Jupiter::SessionApi::GetDataReturnValue>::construct();}}};

				// 						auto it = table.find(methodName);
				// 						if (it == table.end()) {
				// 							return Network::MessageRef();
				// 						}

				// 						return it->second();
				// 					});
				// 		});
				//
			}

			void AccountService::onLogin(const Network::ChannelRef& channel, uint32_t methodId, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = dynamic_cast<Jupiter::AccountService::LoginCommandParameter*>(parameter.get());
				auto response = ProtocolPool<Jupiter::AccountService::LoginCommandReturnValue>::construct();
				response->set_errorcode(Jupiter::AccountService::ErrorCode_Ok);
				Jupiter::RpcErrorCode errorCode = Jupiter::RpcErrorCode_Ok;

				auto it = _playerDataProviderServices.find(realParameter->appid());
				if (it == _playerDataProviderServices.end()) {
					response->set_errorcode(Jupiter::AccountService::ErrorCode_AppIdNotFound);
					errorCode = Jupiter::RpcErrorCode_InvalidParameter;

					completion(errorCode, response);
					return;
				}

				std::shared_ptr<Jupiter::AccountService::UserInfo> info;
				tryLogin(parameter, [&](const std::shared_ptr<Jupiter::AccountService::UserInfo>& userInfo, Jupiter::AccountService::ErrorCode error) {
					if (userInfo == nullptr) {
						errorCode = Jupiter::RpcErrorCode_Exception;
						response->set_errorcode(Jupiter::AccountService::ErrorCode_CannotCreateNewUser);
						completion(errorCode, response);
					} else {
						info = userInfo;
					}
				});

				if (info == nullptr) {
					return;
				}

				_userChannels.insert(std::make_pair(info->userid(), channel));

				_container->acquireService(it->second, [=](const Network::ChannelRef& serviceChannel) {
					initializePlayerGameDataProviderChannel(channel, serviceChannel);

					auto parameter = ProtocolPool<Jupiter::UserDataProvider::GetDataCommandParameter>::construct();
					parameter->set_userid(info->userid());

					serviceChannel->invoke(Jupiter::UserDataProvider::MessageId_GetData, parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef& returnValue) {
						auto result = dynamic_cast<Jupiter::UserDataProvider::GetDataCommandReturnValue*>(returnValue.get());
						info->set_additionaldata(result->data());
						response->set_allocated_detial(info.get());

						auto clientId = boost::uuids::to_string(boost::uuids::random_generator()());
						Services::allocSession(_container, clientId, realParameter->appid(), info->userid(), [=](Jupiter::RpcErrorCode err, int32_t sessionResult, const SessionRef& session) mutable {
							if (err != Jupiter::RpcErrorCode_Ok) {
								errorCode = err;
								if (sessionResult == Jupiter::SessionApi::ErrorCode_SessionAlreadyExist) {
									response->set_errorcode(Jupiter::AccountService::ErrorCode_AlreadyLogin);
									response->set_token(generateToken(session->sessionKey(), realParameter->appid(), info->userid(), clientId));
								} else {
									response->set_errorcode(Jupiter::AccountService::ErrorCode_Ok);
								}
								completion(errorCode, response);
							} else {
								channel->setProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL, session);

								auto parameter = ProtocolPool<Jupiter::SessionApi::SetDataParameter>::construct();
								parameter->set_key("@#account#service#session#data#@");

								auto sessionData = ProtocolPool<Jupiter::AccountService::AccountSessionData>::construct();
								sessionData->set_allocated_info(info.get());

								timeval now;
								evutil_gettimeofday(&now, nullptr);
								sessionData->set_logintime(now.tv_sec);
								sessionData->set_remoteaddress(channel->address());
								sessionData->set_appid(realParameter->appid());
								sessionData->SerializeToString(parameter->mutable_value());
								sessionData->release_info();

								session->updateData("@#account#service#session#data#@", "");

								response->set_token(generateToken(session->sessionKey(), realParameter->appid(), info->userid(), clientId));

								completion(errorCode, response);
							}
						});

						auto sessionServiceChannel = Utilities::any_cast<Network::ChannelRef>(channel->getProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL));

						sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_AllocSession, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef& returnValue) {
							auto result = dynamic_cast<Jupiter::SessionApi::AllocSessionReturnValue*>(returnValue.get());
							if (result->errorcode() == Jupiter::SessionApi::ErrorCode_Ok) {
								auto parameter = ProtocolPool<Jupiter::SessionApi::SetDataParameter>::construct();
								parameter->set_key("@#account#service#session#data#@");

								auto sessionData = ProtocolPool<Jupiter::AccountService::AccountSessionData>::construct();
								sessionData->set_allocated_info(info.get());

								timeval now;
								evutil_gettimeofday(&now, nullptr);
								sessionData->set_logintime(now.tv_sec);
								sessionData->set_remoteaddress(channel->address());
								sessionData->set_appid(realParameter->appid());
								sessionData->SerializeToString(parameter->mutable_value());
								sessionData->release_info();

								sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_SetData, parameter);

								response->set_token(generateToken(result->sessionkey(), realParameter->appid(), info->userid(), clientId));

								completion(errorCode, response);
							} else if (result->errorcode() == Jupiter::SessionApi::ErrorCode_SessionAlreadyExist) {
								errorCode = Jupiter::RpcErrorCode_Exception;
								response->set_errorcode(Jupiter::AccountService::ErrorCode_AlreadyLogin);
								response->set_token(generateToken(result->sessionkey(), realParameter->appid(), info->userid(), clientId));
								completion(errorCode, response);
							}

							response->release_detial();
						});
					});
				});
			}

			void AccountService::onUpdatePlayerData(const Network::ChannelRef& channel, uint32_t methodId, const Network::MessageRef& parameter, const Action<Jupiter::RpcErrorCode, const Network::MessageRef&>& completion) {
				auto realParameter = dynamic_cast<Jupiter::AccountService::UpdatePlayerDataParameter*>(parameter.get());
				auto response = ProtocolPool<Jupiter::AccountService::UpdatePlayerDataReturnValue>::construct();
				response->set_errorcode(Jupiter::AccountService::ErrorCode_Ok);
				Jupiter::RpcErrorCode errorCode = Jupiter::RpcErrorCode_Ok;

				auto getDataParameter = ProtocolPool<Jupiter::SessionApi::GetDataParameter>::construct();
				getDataParameter->set_key("@#account#service#session#data#@");

				auto sessionServiceChannel = Utilities::any_cast<Network::ChannelRef>(channel->getProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL));
				auto userId = Utilities::any_cast<uint64_t>(channel->getProperty(CHANNEL_PROPERTY_USER_ID));

				if (realParameter->has_gender() || realParameter->has_nickname() || realParameter->has_avatarurl()) {
					std::string gender = realParameter->has_gender() ? boost::lexical_cast<std::string>(realParameter->gender()) : "null";
					std::string nickName = realParameter->has_nickname() ? realParameter->nickname() : "null";
					std::string avatarUrl = realParameter->has_avatarurl() ? realParameter->avatarurl() : "null";

					auto query = boost::str(boost::format("call update_user_data(%1%, %2%, %3%, %4%, @isSuccessful)") % userId % gender % nickName % avatarUrl);

					auto&& connection = _dbPool->alloc();

					MySqlStatementPtr state(connection->createStatement());
					state->execute("use " + _dbName);

					state->execute(query);

					MySqlResultSetPtr result(state->executeQuery("select @isSuccessful as isSuccessful"));
					if (!result->getBoolean("isSuccessful")) {
						errorCode = Jupiter::RpcErrorCode_Exception;
						response->set_errorcode(Jupiter::AccountService::ErrorCode_UserNotFound);
						completion(errorCode, response);
						return;
					}
				}

				if (realParameter->has_additionaldata()) {
					auto providerChannel = Utilities::any_cast<Network::ChannelRef>(channel->getProperty(CHANNEL_PROPERTY_GAME_DATA_PROVIDER_CHANNEL));

					auto parameter = ProtocolPool<Jupiter::UserDataProvider::UpdateDataCommandParameter>::construct();
					parameter->set_userid(userId);
					parameter->set_data(realParameter->additionaldata());

					providerChannel->invoke(Jupiter::UserDataProvider::MessageId_UpdateData, parameter);
				} else {
					if (realParameter->has_gender() || realParameter->has_nickname() || realParameter->has_avatarurl()) {
						auto parameter = ProtocolPool<Jupiter::SessionApi::GetDataParameter>::construct();
						parameter->set_key("@#account#service#session#data#@");

						auto sessionServiceChannel = Utilities::any_cast<Network::ChannelRef>(channel->getProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL));
						sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_GetData, parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef& returnValue) {
							auto result = dynamic_cast<Jupiter::SessionApi::GetDataReturnValue*>(returnValue.get());

							auto userInfo = ProtocolPool<Jupiter::AccountService::UserInfo>::construct();
							auto sessionData = ProtocolPool<Jupiter::AccountService::AccountSessionData>::construct();
							sessionData->set_allocated_info(userInfo.get());
							sessionData->ParseFromString(result->value());

							if (realParameter->has_gender()) {
								userInfo->set_gender(realParameter->gender());
							}

							if (realParameter->has_nickname()) {
								userInfo->set_nickname(realParameter->nickname());
							}

							if (realParameter->has_avatarurl()) {
								userInfo->set_avatarurl(realParameter->avatarurl());
							}

							auto parameter = ProtocolPool<Jupiter::SessionApi::SetDataParameter>::construct();
							parameter->set_key("@#account#service#session#data#@");
							sessionData->SerializeToString(parameter->mutable_value());

							sessionData->release_info();

							sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_SetData, parameter);

							channel->send(Jupiter::AccountService::MessageId_UserDataUpdated, userInfo);
						});
					}
				}

				sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_SetData, parameter);
			}

			void AccountService::onClientDisconnected(const Action<>& completion, const Network::ChannelRef& channel) {
				Service::onClientDisconnected(completion, channel);
				channel->setProperty(CHANNEL_PROPERTY_GAME_DATA_PROVIDER_CHANNEL, nullptr);
				channel->setProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL, nullptr);
			}

			void AccountService::onPlayerGameDataUpdatedMessage(const Network::ChannelRef& channel, const Network::NetworkPacketRef& packet, const Network::MessageRef& message) {
				auto realMessage = dynamic_cast<Jupiter::UserDataProvider::PlayerGameDataUpdatedMessage*>(message.get());
				auto it = _userChannels.find(realMessage->userid());
				if (it != _userChannels.end()) {
					auto parameter = ProtocolPool<Jupiter::SessionApi::GetDataParameter>::construct();
					parameter->set_key("@#account#service#session#data#@");

					auto sessionServiceChannel = Utilities::any_cast<Network::ChannelRef>(channel->getProperty(CHANNEL_PROPERTY_SESSSION_SERVICE_CHANNEL));
					sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_GetData, parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef& returnValue) {
						auto result = dynamic_cast<Jupiter::SessionApi::GetDataReturnValue*>(returnValue.get());

						auto userInfo = ProtocolPool<Jupiter::AccountService::UserInfo>::construct();
						auto sessionData = ProtocolPool<Jupiter::AccountService::AccountSessionData>::construct();
						sessionData->set_allocated_info(userInfo.get());
						sessionData->ParseFromString(result->value());

						userInfo->set_additionaldata(realMessage->data());

						auto parameter = ProtocolPool<Jupiter::SessionApi::SetDataParameter>::construct();
						parameter->set_key("@#account#service#session#data#@");
						sessionData->SerializeToString(parameter->mutable_value());

						sessionData->release_info();

						sessionServiceChannel->invoke(Jupiter::SessionApi::MessageId_SetData, parameter);

						it->second->send(Jupiter::AccountService::MessageId_UserDataUpdated, userInfo);
					});
				}
			}

			void AccountService::tryLogin(const Network::MessageRef& parameter, const Action<const std::shared_ptr<Jupiter::AccountService::UserInfo>&, Jupiter::AccountService::ErrorCode>& completion) {
				auto realParameter = dynamic_cast<Jupiter::AccountService::LoginCommandParameter*>(parameter.get());

				auto&& connection = _dbPool->alloc();
				MySqlStatementPtr state(connection->createStatement());
				state->execute("use " + _dbName);

				std::string ref_id;
				std::string token;
				std::string nickName;
				int serviceType = 0;
				auto queryFormat = boost::format("call process_login(%1%, %2%, %3%)");

				switch (realParameter->type()) {
				case Jupiter::AccountService::LoginMethodType_DeviceId: {
					auto data = ProtocolPool<Jupiter::AccountService::LoginCommandParameter::DeviceIdLoginData>::construct();
					data->ParseFromString(realParameter->data());
					ref_id = data->deviceid();
					nickName = boost::str(boost::format("Guest#%1%") % ref_id.substr(0, std::min(ref_id.length(), 10ul)));
				} break;

				case Jupiter::AccountService::LoginMethodType_GameCenter: {
					auto data = ProtocolPool<Jupiter::AccountService::LoginCommandParameter::GameCenterLoginData>::construct();
					data->ParseFromString(realParameter->data());
					ref_id = data->playerid();
					nickName = data->playertag();
				} break;

				case Jupiter::AccountService::LoginMethodType_OAuth: {
					auto data = ProtocolPool<Jupiter::AccountService::LoginCommandParameter::OAuthLoginData>::construct();
					data->ParseFromString(realParameter->data());
					ref_id = data->id();
					token = data->token();
					nickName = ref_id;
					serviceType = ( int )data->type();
				} break;

				default: break;
				}

				queryFormat = queryFormat % ref_id % token % ( int )realParameter->type();

				MySqlResultSetPtr result(state->executeQuery(boost::str(queryFormat)));
				if (result->rowsCount() == 0) {
					queryFormat = boost::format("call process_register(%1%, %2%, %3%, %4%, %5%)") % ref_id % token % ( int )realParameter->type() % serviceType % nickName;
					result.reset(state->executeQuery(boost::str(queryFormat)));

					if (result->rowsCount() == 0) {
						completion(nullptr, Jupiter::AccountService::ErrorCode_CannotCreateNewUser);
					}
				}

				auto info = ProtocolPool<Jupiter::AccountService::UserInfo>::construct();
				info->set_userid(result->getInt64("user_id"));
				info->set_gender(( Jupiter::Jupiter::AccountService::GenderType )result->getUInt("gender"));
				info->set_nickname(result->getString("nick_name"));
				info->set_avatarurl(result->getString("avatar_url"));
				info->set_createtime(result->getInt64("create_time_value"));
				info->set_lastlogintime(result->getInt64("last_login_time_value"));

				completion(info, Jupiter::AccountService::ErrorCode_Ok);
			}

			void AccountService::initializePlayerGameDataProviderChannel(const Network::ChannelRef& clientChannel, const Network::ChannelRef& serviceChannel) {
				_queue.enqueue([=] {
					auto it = std::find(_clients.begin(), _clients.end(), clientChannel);
					if (it == _clients.end()) {
						return;
					}

					clientChannel->setProperty(CHANNEL_PROPERTY_GAME_DATA_PROVIDER_CHANNEL, serviceChannel);

					serviceChannel->closed()->addObserver([this, clientChannel](const Action<>& completion, const Network::ChannelRef& serviceChannel) {
						initializePlayerGameDataProviderChannel(clientChannel, serviceChannel);
						completion();
					});

					serviceChannel->setMethodResponseTranslateHandler([](uint32_t methodId) {
						static std::map<uint32_t, std::function<Network::MessageRef()>> table = { { Jupiter::UserDataProvider::MessageId_GetData, [] {
																									   return ProtocolPool<Jupiter::UserDataProvider::GetDataCommandReturnValue>::construct();
																								   } } };

						auto it = table.find(methodId);
						if (it == table.end()) {
							return Network::MessageRef();
						}

						return it->second();
					});

					serviceChannel->setMessageTranslateHandler([](int messageId) {
						static std::map<int, std::function<Network::MessageRef()>> table = { { Jupiter::UserDataProvider::MessageId_PlayerGameDataUpdated, [] {
																								  return ProtocolPool<Jupiter::UserDataProvider::PlayerGameDataUpdatedMessage>::construct();
																							  } } };

						auto it = table.find(messageId);
						if (it == table.end()) {
							return Network::MessageRef();
						}

						return it->second();
					});

					serviceChannel->setMessageHandlers([this] {
						static std::map<int, std::function<void(const Network::ChannelRef&, const Network::NetworkPacketRef&, const Network::MessageRef&)>> table = { { Jupiter::UserDataProvider::MessageId_PlayerGameDataUpdated, std::bind(&AccountService::onPlayerGameDataUpdatedMessage, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3) } };

						return table;
					});
				});
			}

		}  // namespace Account
	}	   /* namespace Services */
} /* namespace Jupiter */
