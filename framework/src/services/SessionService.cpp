/*
 * @Author: Zhaoming Wang
 * @Date:   2015-11-26
 * @Last Modified by:   Zhaoming Wang
 * @Last Modified time: 2015-12-02
 */

#include "SessionService.h"
#include <jupiter/protocols/SessionApi.pb.h>

namespace Jupiter {
	namespace Services {
		static const std::vector<Network::MessageConfigPtr> st_configs = {
			Network::createMessageConfig<SessionApi::AllocSessionReturnValue>("alloc_session"),
			Network::createMessageConfig<SessionApi::BindSessionToChannelReturnValue>("bind_session_to_channel"),
			Network::createMessageConfig<SessionApi::GetDataReturnValue>("get_data"),
		};

		/**
		 * 创建一个新的session，但是如果给定客户端已经存在一个session，则返回错误。
		 * @param clientId   客户端标识，可能是设备ID之类的
		 * @param appId      应用标识，平台分配的ID
		 * @param userId     用户ID，平台唯一
		 * @param completion
		 * 完成回调，通知操作结果，第一个参数是错误代码，OK表示成功，第二个参数是创建的session对象
		 */
		void allocSession(const ServiceContainerRef &container, const std::string &clientId, const std::string &appId, uint64_t userId, const Action<Jupiter::RpcErrorCode, int32_t, const SessionRef &> &completion) {
			container->acquireService("com.jupiter.services.session", [=](const Network::ChannelRef &serviceChannel) {
				if (serviceChannel != nullptr) {
					serviceChannel->setMessageConfig(st_configs);

					auto parameter = MemPoolAllocater<Jupiter::SessionApi::AllocSessionParameter>::construct();
					parameter->set_appid(appId);
					parameter->set_userid(userId);
					parameter->set_clientid(clientId);

					serviceChannel->invoke("alloc_session", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &returnValue) {
						auto result = dynamic_cast<SessionApi::AllocSessionReturnValue *>(returnValue.get());
						if (result->errorcode() == SessionApi::ErrorCode_Ok) {
							auto session = MemPoolAllocater<Session>::construct(serviceChannel, result->sessionkey());
							completion(errorCode, result->errorcode(), session);
						} else {
							errorCode = Jupiter::RpcErrorCode_Exception;
							completion(errorCode, result->errorcode(), nullptr);
						}
					});
				}
			});
		}

		/**
		 * 恢复一个对话，当一个客户端重新连接时要尝试恢复旧有的session
		 * @param sessionKey 要恢复的session标识
		 * @param completion
		 * 完成回调，通知操作结果，第一个参数是错误代码，OK表示成功，第二个参数是创建的session对象
		 */
		void restoreSession(const ServiceContainerRef &container, const std::string &sessionKey, const Action<Jupiter::RpcErrorCode, int32_t, const SessionRef &> &completion) {
			container->acquireService("com.jupiter.services.session", [=](const Network::ChannelRef &serviceChannel) {
				if (serviceChannel != nullptr) {
					serviceChannel->setMessageConfig(st_configs);

					auto parameter = MemPoolAllocater<Jupiter::SessionApi::BindSessionToChannelParameter>::construct();
					parameter->set_sessionkey(sessionKey);

					serviceChannel->invoke("bind_session_to_channel", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &returnValue) {
						auto result = dynamic_cast<SessionApi::AllocSessionReturnValue *>(returnValue.get());
						if (result->errorcode() == SessionApi::ErrorCode_Ok) {
							auto session = MemPoolAllocater<Session>::construct(serviceChannel, result->sessionkey());
							completion(errorCode, result->errorcode(), session);
						} else {
							errorCode = Jupiter::RpcErrorCode_Exception;
							completion(errorCode, result->errorcode(), nullptr);
						}
					});
				}
			});
		}

		Session::Session(const Network::ChannelRef &channel, const std::string &key)
			: _serviceChannel(channel)
			, _sessionKey(key) {
			_serviceChannel->closed()->addObserver([this](const Action<> &completion, const Network::ChannelRef &channel) {
				_serviceChannel = nullptr;
				_disconnected->fire(completion, shared_from_this());
				completion();
			});
		}

		Session::~Session() {
		}

		/**
		 * session标识，唯一标识一个session。
		 * @return  标识符
		 */
		const std::string &Session::sessionKey(void) const {
			return _sessionKey;
		}

		/**
		 * session连接断开事件。
		 */
		const std::shared_ptr<Utilities::ObserverHub<const SessionRef &>> &Session::disconnected() const {
			return _disconnected;
		}

		/**
		 * 恢复一个对话，当一个客户端重新连接时要尝试恢复旧有的session
		 * @param container  容器对象
		 * @param completion
		 * 完成回调，通知操作结果，第二个参数是Jupiter::SessionApi::ErrorCode类型的错误代码，OK表示成功
		 */
		void Session::restore(const ServiceContainerRef &container, const Action<Jupiter::RpcErrorCode, int32_t> &completion) {
			if (_serviceChannel != nullptr) {
				completion(Jupiter::RpcErrorCode_Exception, 0);
				return;
			}

			container->acquireService("com.jupiter.services.session", [=](const Network::ChannelRef &serviceChannel) {
				if (serviceChannel != nullptr) {
					serviceChannel->setMessageConfig(st_configs);

					auto parameter = MemPoolAllocater<Jupiter::SessionApi::BindSessionToChannelParameter>::construct();
					parameter->set_sessionkey(_sessionKey);

					serviceChannel->invoke("bind_session_to_channel", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &returnValue) {
						auto result = dynamic_cast<SessionApi::AllocSessionReturnValue *>(returnValue.get());
						if (result->errorcode() == SessionApi::ErrorCode_Ok) {
							_serviceChannel = serviceChannel;
							_sessionKey = result->sessionkey();
						} else {
							errorCode = Jupiter::RpcErrorCode_Exception;
						}
						completion(errorCode, result->errorcode());
					});
				}
			});
		}

		/**
		 * 更新指定值，如果该值已经存在，则用新的值代替，否则创建新的入口。
		 * @param key  值标识
		 * @param data 值内容
		 * @return 成功返回true，如果连接断开返回false。
		 */
		bool Session::updateData(const std::string &key, const boost::any &data) {
			if (_serviceChannel == nullptr) {
				return false;
			}

			auto parameter = ProtocolPool<SessionApi::SetDataParameter>::construct();
			parameter->set_key(key);

			auto value = parameter->mutable_value();
			const auto &valueType = data.type();
			if (valueType == typeid(uint32_t)) {
				value->set_intvalue(Jupiter::Utilities::any_cast<uint32_t>(data));
			} else if (valueType == typeid(uint64_t)) {
				value->set_longvalue(Jupiter::Utilities::any_cast<uint64_t>(data));
			} else if (valueType == typeid(bool)) {
				value->set_boolvalue(Jupiter::Utilities::any_cast<bool>(data));
			} else if (valueType == typeid(double)) {
				value->set_doublevalue(Jupiter::Utilities::any_cast<double>(data));
			} else if (valueType == typeid(std::string)) {
				value->set_stringvalue(Jupiter::Utilities::any_cast<std::string>(data));
			}


			_serviceChannel->invoke("set_data", parameter);
			return true;
		}

		/**
		 * 获取给定值的内容，该过程是异步的。
		 * @param key        值标识
		 * @param completion 通知回调
		 */
		void Session::getData(const std::string &key, const Action<int, const boost::any &> &completion) const {
			if (_serviceChannel == nullptr) {
				completion(-1, "");
			}

			auto parameter = ProtocolPool<SessionApi::GetDataParameter>::construct();
			parameter->set_key("@#account#service#session#data#@");

			_serviceChannel->invoke("get_data", parameter, [=](Jupiter::RpcErrorCode errorCode, const Network::MessageRef &returnValue) {
				auto result = dynamic_pointer_cast<SessionApi::GetDataReturnValue>(returnValue);
				auto valueCase = result->value().Value_case();
				boost::any value;
				switch (valueCase) {
				case Jupiter::Common::CommonDataValue::ValueCase::kIntValue:
					value = result->value().intvalue();
					break;
				case Jupiter::Common::CommonDataValue::ValueCase::kLongValue:
					value = result->value().longvalue();
					break;
				case Jupiter::Common::CommonDataValue::ValueCase::kBoolValue:
					value = result->value().boolvalue();
					break;
				case Jupiter::Common::CommonDataValue::ValueCase::kDoubleValue:
					value = result->value().doublevalue();
					break;
				case Jupiter::Common::CommonDataValue::ValueCase::kStringValue:
					value = result->value().stringvalue();
					break;

				default:
					break;
				}

				completion(result->errorcode(), value);
			});
		}

		/**
		 * 删除一个值
		 * @param key 值标识
		 */
		void Session::removeData(const std::string &key) {
			if (_serviceChannel == nullptr) {
				return;
			}

			auto parameter = ProtocolPool<SessionApi::SetDataParameter>::construct();
			parameter->set_key(key);

			_serviceChannel->invoke("set_data", parameter);
		}
	}  // namespace Services
}  // namespace Jupiter