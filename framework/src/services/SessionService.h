/*
 * @Author: Zhaoming Wang
 * @Date:   2015-11-26
 * @Last Modified by:   Zhaoming Wang
 * @Last Modified time: 2015-12-01
 */

#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Services {
		class Session : public ISession, public std::enable_shared_from_this<Session> {
		public:
			Session(const Network::ChannelRef &channel, const std::string &key);
			~Session();

			/**
			 * session标识，唯一标识一个session。
			 * @return  标识符
			 */
			virtual const std::string &sessionKey(void) const override;

			/**
			 * session连接断开事件。
			 */
			virtual const std::shared_ptr<Utilities::ObserverHub<const SessionRef &>> &disconnected() const override;

			/**
			 * 恢复一个对话，当一个客户端重新连接时要尝试恢复旧有的session
			 * @param container  容器对象
			 * @param completion
			 * 完成回调，通知操作结果，第二个参数是Jupiter::Protocols::SessionApi::ErrorCode类型的错误代码，OK表示成功
			 */
			virtual void restore(const ServiceContainerRef &container, const Action<Jupiter::RpcErrorCode, int32_t> &completion) override;

			/**
			 * 更新指定值，如果该值已经存在，则用新的值代替，否则创建新的入口。
			 * @param key  值标识
			 * @param data 值内容
			 * @return 成功返回true，如果连接断开返回false。
			 */
			virtual bool updateData(const std::string &key, const boost::any &data) override;

			/**
			 * 获取给定值的内容，该过程是异步的。
			 * @param key        值标识
			 * @param completion 通知回调
			 */
			virtual void getData(const std::string &key, const Action<int32_t, const boost::any &> &completion) const override;

			/**
			 * 删除一个值
			 * @param key 值标识
			 */
			virtual void removeData(const std::string &key) override;

		private:
			std::shared_ptr<Utilities::ObserverHub<const SessionRef &>> _disconnected = MemPoolAllocater<Utilities::ObserverHub<const SessionRef &>>::construct();
			Network::ChannelRef _serviceChannel;
			std::string _sessionKey;
		};
	}  // namespace Services
}  // namespace Jupiter