/*
 * OperationQueue.cpp
 *
 *  Created on: Sep 8, 2014
 *      Author: unalone
 */

#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Utilities {

		OperationQueue OperationQueue::_mainQueue;
		std::atomic_ulong OperationQueue::_queueSeed;

		OperationQueue::OperationQueue (void) :
				OperationQueue(nullptr) {
		}

		OperationQueue::OperationQueue (dispatch_queue_t queue) {
			if (queue == nullptr) {
				_queue = dispatch_queue_create(nullptr, nullptr);
			} else {
#if !TARGET_OS_MAC
				dispatch_retain(queue);
#endif
				_queue = queue;
			}

			long queueId = (long) dispatch_get_context(_queue);
			if (queueId != 0) {
				_queueId = queueId;
			} else {
				_queueId = _queueSeed.fetch_add(1);
				dispatch_set_context(_queue, &_queueId);
			}
		}

		OperationQueue::~OperationQueue () {
#if !TARGET_OS_MAC
			dispatch_release(_queue);
#endif
		}

		const OperationQueue& OperationQueue::mainQueue () {
			return _mainQueue;
		}

		void OperationQueue::enqueue(dispatch_block_t operation) const {
			dispatch_async(_queue, operation );
		}

		void OperationQueue::enqueue(dispatch_block_t operation, float time) const {
			if (time != 0) {
				dispatch_time_t dtime = dispatch_time(0, (int64_t)(time * 1e9));
				dispatch_after(dtime, _queue, operation);
			} else {
				dispatch_async(_queue, operation);
			}
		}

		void OperationQueue::call(dispatch_block_t operation) const {
			long queueId = ( long )dispatch_get_context(_queue);
			if (queueId == _queueId) {
				operation();
			} else {
				dispatch_sync(_queue, operation);
			}
		}
	} /* namespace Utilities */
} /* namespace Dreammatrix */
