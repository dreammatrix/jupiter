#include "jupiter/pool/memory_pool.h"
#include <algorithm>
#include <cstring>
#include <jupiter/utilities/Crypto.h>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <string.h>
#include <string>

namespace Jupiter {
	namespace Utilities {
		unsigned char Crypto::_iv[ AES_BLOCK_SIZE ];

		void Crypto::initialize(void) {
			RAND_priv_bytes(Crypto::_iv, sizeof(Crypto::_iv));
		}

		const std::string &&Crypto::sha256Hash(const std::string &buffer) {
			EVP_MD_CTX *mdctx = EVP_MD_CTX_create();
			EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL);
			EVP_DigestUpdate(mdctx, buffer.c_str(), buffer.length());

			unsigned char md_value[ EVP_MAX_MD_SIZE ];
			unsigned int md_len;
			EVP_DigestFinal_ex(mdctx, md_value, &md_len);
			EVP_MD_CTX_destroy(mdctx);

			return std::move(std::string(( char * )md_value, md_len));
		}

		const std::string &&Crypto::aesCrypto(const std::string &buffer, const std::string &key) {
			EVP_CIPHER_CTX *ctx;
			if (!(ctx = EVP_CIPHER_CTX_new())) {
				return std::move(std::string());
			}

			unsigned char iv[ AES_BLOCK_SIZE ];
			memcpy(iv, _iv, sizeof(_iv));
			if (1 != EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, ( const unsigned char * )key.c_str(), iv)) {
				return std::move(std::string());
			}

			int len;
			unsigned char ciphertext[(buffer.length() / 16 + 1) * 16];
			if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, ( const unsigned char * )buffer.c_str(), buffer.length())) {
				return std::move(std::string());
			}
			auto ciphertext_len = len;

			if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len)) {
				return std::move(std::string());
			}
			ciphertext_len += len;

			EVP_CIPHER_CTX_free(ctx);

			return std::move(std::string(( char * )ciphertext, ciphertext_len));
		}

		const std::string &&Crypto::aesDecrypto(const std::string &buffer, const std::string &key) {
			EVP_CIPHER_CTX *ctx;
			int len;
			int plaintext_len;

			/* Create and initialise the context */
			if (!(ctx = EVP_CIPHER_CTX_new())) {
				return std::move(std::string());
			}


			/* Initialise the decryption operation. IMPORTANT - ensure you use a key
			 * and IV size appropriate for your cipher
			 * In this example we are using 256 bit AES (i.e. a 256 bit key). The
			 * IV size for *most* modes is the same as the block size. For AES this
			 * is 128 bits */
			unsigned char iv[ AES_BLOCK_SIZE ];
			memcpy(iv, _iv, sizeof(_iv));
			if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, ( unsigned char * )key.c_str(), iv)) {
				return std::move(std::string());
			}


			/* Provide the message to be decrypted, and obtain the plaintext output.
			 * EVP_DecryptUpdate can be called multiple times if necessary
			 */
			unsigned char plaintext[1024];
			if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ( unsigned char * )buffer.c_str(), buffer.length())) {
				return std::move(std::string());
			}

			plaintext_len = len;

			/* Finalise the decryption. Further plaintext bytes may be written at
			 * this stage.
			 */
			if (1 != EVP_DecryptFinal_ex(ctx, plaintext + len, &len)) {
				return std::move(std::string());
			}

			plaintext_len += len;

			/* Clean up */
			EVP_CIPHER_CTX_free(ctx);

			return std::move(std::string(( char * )plaintext, plaintext_len));
		}
	}  // namespace Utilities
}  // namespace Jupiter
