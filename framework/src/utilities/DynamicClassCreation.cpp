#include <jupiter/jupiter.h>

namespace Jupiter {
	namespace Utilities {

		dynamic_class_table* g_classheader = 0;

		// =================================================================
		// dynamic_class_table
		// =================================================================

		dynamic_class_table::dynamic_class_table(const char* name, pfn_classcreate func, dynamic_class_table** header)
				: m_class_it(NULL) {
			m_name = name;
			m_pfn_create = func;

//			OutputDebugStringA(name);
//			OutputDebugStringA("\n");

			// 使用全局头
			if (header == 0) {
				if (g_classheader != 0)
					m_next = g_classheader;

				g_classheader = this;
			} else {
				if (header != 0)
					m_next = *header;

				*header = this;
			}
		}

		void* dynamic_class_table::instance_create() {
			return m_pfn_create();
		}

		void* dynamic_class_create(const char* name, dynamic_class_table* header) {
			dynamic_class_table* ret = dynamic_class_query(name, header);
			if (ret == 0)
				return 0;

			return ret->instance_create();
		}

		dynamic_class_table* dynamic_class_query(const char* name, dynamic_class_table* header) {
			// 使用全局头
			if (header == 0)
				header = g_classheader;

			dynamic_class_table* classtable = header;

			while (classtable != 0) {
				if (!strcmp(name, classtable->m_name))
					return classtable;

				classtable = classtable->m_next;
			}

			return 0;
		}

		int dynamic_class_iterate(pfn_classcallback pfncallback, dynamic_class_table* header, void* userdata) {
			// 使用全局头
			if (header == 0)
				header = g_classheader;

			dynamic_class_table* classtable = header;

			int number = 0;
			while (classtable != 0) {
				if (pfncallback) {
					pfncallback(classtable->m_name, header, userdata);
					number++;
				}

				classtable = classtable->m_next;
			}

			return number;
		}

		dynamic_class_table* dynamic_class_first(dynamic_class_table* header) {
			if (header == 0)
				header = g_classheader;

			if (header == 0)
				return 0;

			header->m_class_it = header;

			return dynamic_class_next(header);
		}

		dynamic_class_table* dynamic_class_next(dynamic_class_table* header) {
			if (header == 0)
				header = g_classheader;

			if (header == 0)
				return 0;

			if (header->m_class_it == 0)
				return 0;

			dynamic_class_table* ret = header->m_class_it;
			header->m_class_it = header->m_class_it->m_next;

			return ret;
		}

		int dynamic_class_count(dynamic_class_table* header /*= 0 */) {
			// 使用全局头
			if (header == 0)
				header = g_classheader;

			dynamic_class_table* classtable = header;

			int number = 0;
			while (classtable != 0) {
				number++;
				classtable = classtable->m_next;
			}

			return number;
		}

	}  // namespace Utilities
}
