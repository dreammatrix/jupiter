// /*
//  * DbConnectionPool.cpp
//  *
//  *  Created on: Mar 13, 2015
//  *      Author: unalone
//  */

// #include <jupiter/pool/DbConnectionPool.h>

// namespace Jupiter {

// 	DbConnectionPool *DbConnectionPool::st_sharedInstance = nullptr;

// 	DbConnectionPool::~DbConnectionPool () {
// 		for (auto connection : _connections) {
// 			try {
// 				connection->close();
// 			} catch (sql::SQLException &e) {
// 				perror(e.what());
// 			} catch (std::exception &e) {
// 				perror(e.what());
// 			}
// 			delete connection;
// 		}
// 	}

// 	const MySqlConnectionRef &&DbConnectionPool::alloc (void) {
// 		sql::Connection *connection = nullptr;
// 		_queue.call([&] () {
// 			if (_connections.empty()) {
// 				if (_count < _maxSize) {
// 					sql::Connection *connection = createConnection();
// 					if (connection == nullptr) {
// 						return;
// 					}
// 				} else {
// 					return;
// 				}
// 			}

// 			connection = _connections.back();
// 			_connections.pop_back();

// 			if(connection->isClosed()) {
// 				delete connection;
// 				connection = createConnection();

// 			}
// 		});

// 		if (connection == nullptr) {
// 			return std::move(MySqlConnectionRef());
// 		}

// 		auto object = std::shared_ptr<sql::Connection>(connection, std::bind(&DbConnectionPool::free, this, std::placeholders::_1));
// 		return std::move(object);
// 	}

// 	bool DbConnectionPool::init (const std::string &url, const std::string &userName, const std::string &password, int maxSize) {
// 		_url = url;
// 		_userName = userName;
// 		_password = password;
// 		_maxSize = maxSize;
// 		_count = maxSize / 2;

// 		for (int index = 0; index < _count; ++index) {
// 			if (createConnection() == nullptr) {
// 				return false;
// 			}
// 		}

// 		return true;
// 	}

// 	void DbConnectionPool::free (sql::Connection* connection) {
// 		_queue.enqueue([=] {
// 			_connections.push_back(connection);
// 		});
// 	}

// 	sql::Connection* DbConnectionPool::createConnection () {
// 		sql::Connection *connection = nullptr;
// 		try {
// 			connection = get_driver_instance()->connect(_url, _userName, _password); //建立连接
// 			_connections.push_back(connection);
// 			return connection;
// 		} catch (sql::SQLException&e) {
// 			perror("创建连接出错");
// 			return nullptr;
// 		} catch (std::runtime_error&e) {
// 			perror("运行时出错");
// 			return nullptr;
// 		}
// 	}

// } /* namespace Jupiter */
