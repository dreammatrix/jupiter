/*
 * Channel.cpp
 *
 *  Created on: Sep 1, 2014
 *      Author: unalone
 */

#include "Channel.h"
#include "Connection.h"
#include "jupiter/protocols/Network.pb.h"
#include <jupiter/protocols/Connection.pb.h>
#include <string>
#include <sys/socket.h>
#include <tuple>
#include <utility>
#include <variant>

namespace Jupiter {
	namespace Network {

		Channel::Channel(const ConnectionRef& connection, int id)
			: _connection(connection)
			, _channelId(id)
			, _address(_connection->address())
			, _alternativeAddress(std::make_tuple("", -1)) {
		}

		Channel::~Channel() {
		}

		uint16_t Channel::channelId() const {
			return _channelId;
		}

		const std::tuple<std::string, short>& Channel::address(void) const {
			return _address;
		}

		const std::tuple<std::string, short>& Channel::alternativeAddress(void) const {
			return _alternativeAddress;
		}
		void Channel::setAlternativeAddress(const std::tuple<std::string, short>& value) {
			_alternativeAddress = value;

			auto message = ProtocolPool<Jupiter::Connection::SetChannelRealAddressMessage>::construct();
			message->set_channelid(_channelId);

			auto [ address, port ] = _address;
			message->set_address(address);
			message->set_port(port);

			dynamic_cast<Connection*>(_connection.get())->sendControlPacket(Jupiter::Connection::MessageId_SetChannelRealAddress, message);
		}

		void Channel::setMessageConfig(const std::vector<MessageConfigPtr>& configs) {
			_messageConfigs = configs;
		}

		const ConnectionRef& Channel::connection() const {
			return _connection;
		}

		const std::shared_ptr<Utilities::ObserverHub<const ChannelRef&>>& Channel::closed() const {
			return _closed;
		}

		const std::shared_ptr<Utilities::ObserverHub<const ChannelRef&, const NetworkPacketRef&>>& Channel::dataArrived() const {
			return _dataArrived;
		}

		void Channel::close(const std::function<void()>& completion) {
			_connection.reset();
			_closed->fire(completion, shared_from_this());
		}

		void Channel::send(int messageId) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			send(messageId, nullptr);
		}

		void Channel::send(int messageId, const MessageRef& message) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			auto packet = ProtocolPool<NetworkPacket>::construct();
			packet->set_messageid(messageId);

			if (message != nullptr) {
				message->SerializeToString(packet->mutable_payload());
			}

			send(packet);
		}

		void Channel::send(const Network::NetworkPacketRef& packet) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			if (packet->body_case() == Jupiter::NetworkPacket::BodyCase::kMessageId) {
				printf("Sending message to remote, length = %d, message id = %d on channel %d.\n", ( int16_t )packet->ByteSize(), packet->messageid(), _channelId);
			} else if (packet->body_case() == Jupiter::NetworkPacket::BodyCase::kMethodName) {
				printf("Calling remote method: %s, length = %d, on channel %d.\n", packet->methodname().c_str(), ( int16_t )packet->ByteSize(), _channelId);
			} else {
				printf("Returning data from method: %s, length = %d, on channel %d.\n", packet->methodname().c_str(), ( int16_t )packet->ByteSize(), _channelId);
			}

			dynamic_cast<Connection*>(_connection.get())->send(_channelId, packet);
		}

		void Channel::invoke(const std::string& methodName) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			invoke(methodName, nullptr, nullptr);
		}

		void Channel::invoke(const std::string& methodName, const MessageRef& parameter) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			invoke(methodName, parameter, nullptr);
		}

		void Channel::invoke(const std::string& methodName, const Action<RpcErrorCode, const MessageRef&>& completion) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			invoke(methodName, nullptr, completion);
		}

		void Channel::invoke(const std::string& methodName, const MessageRef& parameter, const Action<RpcErrorCode, const MessageRef&>& completion) {
			if (_connection == nullptr) {
				throw new ChannelStatusException();
			}

			auto packet = ProtocolPool<NetworkPacket>::construct();
			packet->set_methodname(methodName);
			packet->set_requestid(_rpcCallSequenceId.fetch_add(1));

			if (parameter != nullptr) {
				parameter->SerializeToString(packet->mutable_payload());
			}

			if (completion != nullptr) {
				_responseHandlers.insert(std::make_pair(packet->requestid(), std::make_pair(methodName, completion)));
			}

			send(packet);
		}

		void Channel::processMessage(const NetworkPacketRef& packet) {
			std::string address;
			short port;
			if (std::get<1>(_alternativeAddress) == -1) {
				std::tie(address, port) = _address;
			} else {
				std::tie(address, port) = _alternativeAddress;
			}
			printf("Data arrived from %s:%d, over channel %d.\n", address.c_str(), port, _channelId);
			_dataArrived->fire(
				[=]() {
					if (packet->body_case() == Jupiter::NetworkPacket::BodyCase::kMessageId) {
						auto it = std::find_if(_messageConfigs.begin(), _messageConfigs.end(), [&](const auto& c) {
							return std::holds_alternative<int32_t>(c->id()) && std::holds_alternative<MessageHandlerType>(c->handler()) && std::get<int32_t>(c->id()) == packet->messageid();
						});
						if (it == _messageConfigs.end()) {
							return;
						}

						printf("Message arrived from %s:%d, over channel %d, message id: %d.\n", address.c_str(), port, _channelId, packet->messageid());
						MessageRef message;
						try {
							if (!packet->payload().empty()) {
								message = (*it)->createIncomingMessageInstance();
								// MessageRef message = _messageTranslateHandler(packet->messageid());
								if (message == nullptr) {
									return;
								}
								message->ParseFromString(packet->payload());
							}
						} catch (...) { message.reset(); }

						if (message != nullptr) {
							auto handler = std::get<MessageHandlerType>((*it)->handler());	//_messageHandlers().find(packet->messageid());
							handler(shared_from_this(), packet, message);
						}
					} else if (packet->body_case() == Jupiter::NetworkPacket::BodyCase::kMethodName) {
						auto it = std::find_if(_messageConfigs.begin(), _messageConfigs.end(), [&](const auto& c) {
							return std::holds_alternative<std::string>(c->id()) && std::holds_alternative<RpcMethodType>(c->handler()) && std::get<std::string>(c->id()) == packet->methodname();
						});
						if (it == _messageConfigs.end()) {
							return;
						}

						MessageRef parameter;
						if (!packet->payload().empty()) {
							parameter = (*it)->createIncomingMessageInstance();
							// MessageRef message = _messageTranslateHandler(packet->messageid());
							if (parameter == nullptr) {
								return;
							}
							parameter->ParseFromString(packet->payload());
						}

						printf("Calling method: %s, length = %d, on channel %d.\n", packet->methodname().c_str(), parameter == nullptr ? 0 : ( int16_t )parameter->ByteSize(), _channelId);
						auto handler = std::get<RpcMethodType>((*it)->handler());
						handler(shared_from_this(), packet->methodname(), parameter, [=](RpcErrorCode errorCode, const MessageRef& result) {
							auto response = ProtocolPool<NetworkPacket>::construct();
							response->set_errorcode(errorCode);
							response->set_requestid(packet->requestid());

							if (result != nullptr) {
								result->SerializePartialToString(response->mutable_payload());
							}

							printf("Returning method response for method: %s, length = %d, on channel %d.\n", packet->methodname().c_str(), ( int16_t )response->ByteSize(), _channelId);

							send(response);
						});
					} else if (packet->body_case() == Jupiter::NetworkPacket::BodyCase::kErrorCode) {
						auto it = _responseHandlers.find(packet->requestid());
						if (it == _responseHandlers.end()) {
							return;
						}

						MessageRef result;
						if (!packet->payload().empty()) {
							auto cit = std::find_if(_messageConfigs.begin(), _messageConfigs.end(), [&](const auto& c) {
								return std::holds_alternative<std::string>(c->id()) && std::holds_alternative<MessageHandlerType>(c->handler()) &&
									   std::get<std::string>(c->id()) == it->second.first;
							});
							if (cit == _messageConfigs.end()) {
								return;
							}

							result = (*cit)->createReturnMessageInstance();
							if (result != nullptr) {
								result->ParseFromString(packet->payload());
							}
						}

						printf("Method: \"%s\" returned, length = %d, on channel %d.\n", packet->methodname().c_str(), result == nullptr ? 0 : ( int16_t )result->ByteSize(), _channelId);
						it->second.second(packet->errorcode(), result);

						_responseHandlers.erase(it);
					}
				},
				shared_from_this(), packet);
		}

		/**
		 * 设置属性的值。
		 * @param name		属性名。
		 * @param value		待设置的值。
		 */
		void Channel::setProperty(const std::string& name, const boost::any& value) {
			_properties.insert_or_assign(name, value);
		}

		/**
		 * 获取属性的值。
		 * @param name		属性名。
		 * @return 如果存在这个属性，则返回属性值，否则返回空any。
		 */
		boost::any Channel::getProperty(const std::string& name) const {
			auto it = _properties.find(name);
			if (it == _properties.end()) {
				return boost::any();
			}

			dispatch_time_t dtime = dispatch_time(0, (int64_t)(1e9));
			dispatch_after(dtime, dispatch_get_main_queue(), ^{
				for (int index = 0; index < 10; ++index) {}
			});

			return it->second;
		}

		/**
		 * 查询是否存在一个属性。
		 * @param name		属性名。
		 * @return 如果存在这个属性则返回true，否则返回false。
		 */
		bool Channel::hasProperty(const std::string& name) const {
			auto it = _properties.find(name);
			return it != _properties.end();
		}

	} /* namespace Network */
}  // namespace Jupiter
