/*
 * Connection.h
 *
 *  Created on: Jun 16, 2014
 *      Author: unalone
 */

#ifndef CONNECTION_IMPL_H_
#define CONNECTION_IMPL_H_

#include "jupiter/network/Types.h"
#include "jupiter/protocols/Connection.pb.h"
#include <atomic>
#include <bits/stdint-uintn.h>
#include <condition_variable>
#include <cstdint>
#include <cstring>
#include <event2/bufferevent.h>
#include <event2/event.h>
#include <event2/listener.h>
#include <event2/util.h>
#include <functional>
#include <jupiter/jupiter.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

namespace Jupiter {
	namespace Network {

		class Channel;
		class Connection : public std::enable_shared_from_this<Connection>, public IConnection {
		public:
			Connection();
			virtual ~Connection();

			virtual uint64_t id(void) const override;

			virtual const std::tuple<std::string, short> &address(void) const override;

			virtual const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> connectionArrived(void) const override;
			virtual const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> connectionClosed(void) const override;

			virtual void setPreDispatcher(const Method<std::tuple<ConnectionRef, int>, const ConnectionRef &, int> &processor) override;
			virtual void setChannelInitializer(const Action<const ConnectionRef &, const ChannelRef &> &handler) override;

			virtual ChannelRef channelOfId(int channelId) const override;

			virtual ChannelRef fork() override;

			virtual void close(void) override;

			bool listenOnAdapter(const std::string &adapterName, ushort port);
			bool listenOnLocalSocketFile(const std::string &path);
			void connectToServer(const std::string &address, ushort port, const Action<bool> &completion);
			void connectToServer(const std::string &path, const Action<bool> &completion);

			void send(int channelId, const Network::MessageRef &packet);
			void sendRaw(int channelId, char *const buffer, int length);
			void sendControlPacket(int messageId, const MessageRef &message = nullptr);

			/**
			 * 设置属性的值。
			 * @param name		属性名。
			 * @param value		待设置的值。
			 */
			virtual void setProperty(const std::string &name, const boost::any &value) override;

			/**
			 * 获取属性的值。
			 * @param name		属性名。
			 * @return 如果存在这个属性，则返回属性值，否则返回空any。
			 */
			virtual boost::any getProperty(const std::string &name) const override;

			/**
			 * 查询是否存在一个属性。
			 * @param name		属性名。
			 * @return 如果存在这个属性则返回true，否则返回false。
			 */
			virtual bool hasProperty(const std::string &name) const override;

		private:
			enum class Status { Idle, Connecting, Connected, Listening, Closing, Destroyed };
			enum class Direction { None, In, Out };

			static std::atomic_uint_fast64_t _connectionId;
			uint64_t _id;

			Status _status = Status::Idle;
			Direction _direction = Direction::None;

			Action<bool> _connectToServerCompletion;

			Jupiter::Utilities::OperationQueue _queue;
			Jupiter::Utilities::OperationQueue _sendingQueue;

			evutil_socket_t _socket = -1;
			std::tuple<std::string, short> _address;

			std::map<uint16_t, std::shared_ptr<Channel>> _channels;
			std::atomic_uint16_t _channelId;
			std::mutex _mutex;
			std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> _connectionArrived = MemPoolAllocater<Utilities::ObserverHub<const ConnectionRef &>>::construct();
			std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> _connectionClosed = MemPoolAllocater<Utilities::ObserverHub<const ConnectionRef &>>::construct();

			Method<std::tuple<ConnectionRef, int>, const ConnectionRef &, int> _processor;
			Action<const ConnectionRef &, const ChannelRef &> _channelInitializer;

			std::map<std::string, boost::any> _properties;

			Utilities::CircleBufferRef _reciveBuffer = MemPoolAllocater<Utilities::CircleBuffer>::construct(4096);

			class SendingBuffer {
			public:
				SendingBuffer(int channelId, const MessageRef &packet)
					: _length(( int16_t )packet->ByteSize()) {
					_buffer = ( char * )MemoryPool::get_instance()->alloc(sizeof(int16_t) * 2 + _length);

					auto value = htons(_length);
					memcpy(_buffer, &value, sizeof(int16_t));

					value = htons(channelId);
					memcpy(_buffer + sizeof(int16_t), &value, sizeof(int16_t));

					packet->SerializeToArray(_buffer + sizeof(int16_t) * 2, _length);

					_length += sizeof(int16_t) * 2;
				}

				SendingBuffer(int channelId, char *const buffer, int length)
					: _length(length) {
					_buffer = ( char * )MemoryPool::get_instance()->alloc(sizeof(int16_t) * 2 + _length);

					auto value = htons(_length);
					memcpy(_buffer, &value, sizeof(int16_t));

					value = htons(channelId);
					memcpy(_buffer + sizeof(int16_t), &value, sizeof(int16_t));

					memcpy(_buffer + sizeof(int16_t) * 2, buffer, _length);

					_length += sizeof(int16_t) * 2;
				}

				~SendingBuffer() {
					if (_buffer != nullptr) {
						MemoryPool::get_instance()->free(_buffer);
						_buffer = nullptr;
					}
				}

				bool send(int socketfd) {
					int offset = 0;

					do {
						auto ret = ::send(socketfd, _buffer + offset, _length - offset, 0);
						if (ret == -1) {
							return false;
						}

						offset += ret;
						if (offset >= _length) {
							return true;
						}
					} while (true);
				}

			private:
				char *_buffer = nullptr;
				int16_t _length = 0;
			};

			typedef std::shared_ptr<SendingBuffer> SendingBufferRef;
			std::mutex _sendingMutex;
			std::list<SendingBufferRef> _sendingBuffers;
			std::atomic_bool _isSending;

			event *_socketEvent = nullptr;
			evconnlistener *_listener = nullptr;
			bufferevent *_bev = nullptr;

			std::map<int, std::function<void(const std::shared_ptr<Jupiter::Connection::ControlPacket> &)>> _controlMessageHandlers;

			void initWithSocket(evconnlistener *listener, int socket);

			void onConnectionClosed(void);
			void onChannelClosed(const Action<> &completion, const ChannelRef &channel);
			void onConnectedToServer(void);

			void onDataArrived(int length, char *buffer);
			// void onDataArrived(evbuffer *buffer);
			void flushSendingBuffers();

			static void eventCallback(evutil_socket_t socket, short what, void *data);

			static void onClientConnected(struct evconnlistener *listener, evutil_socket_t sock, struct sockaddr *addr, int len, void *ptr);
			// static void onSocketCanRead(struct bufferevent *bev, void *ctx);
			// static void onSocketCanWrite(struct bufferevent *bev, void *ctx);
			static void onSocketEventHappend(struct bufferevent *bev, short events, void *ctx);

			static bool _needStop;

			std::shared_ptr<Channel> fork(int channelId);
			void removeChannel(int channelId);
			void freeChannelPtr(Channel *ptr);

			void processControlPacket(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet);

			void onRemoteChannelClosedMessage(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet);
			void onSetChannelRealAddressMessage(const std::shared_ptr<Jupiter::Connection::ControlPacket> &packet);
		};

	} /* namespace Network */
}  // namespace Jupiter

#endif /* CONNECTION_IMPL_H_ */
