/*
 * EventLoop.cpp
 *
 *  Created on: Mar 17, 2015
 *      Author: unalone
 */

#include <jupiter/core/EventLoop.h>

namespace Jupiter {

	EventLoop* EventLoop::_mainLoop = nullptr;

	EventLoop::EventLoop() {
		_eventBase = event_base_new();
	}

	EventLoop::~EventLoop() {
	}

	EventLoop* EventLoop::mainLoop() {
		if (_mainLoop == nullptr) {
			_mainLoop = new EventLoop();
		}
		return _mainLoop;
	}

	event_base* EventLoop::eventBase() const {
		return _eventBase;
	}

	void EventLoop::run(void) {
		event_base_loop(_eventBase, EVLOOP_NO_EXIT_ON_EMPTY);
	}

	void EventLoop::runInThread(void) {
		_eventLoopThread = std::thread(&EventLoop::eventLoopThreadEntry);
	}

	void EventLoop::stop() {
		event_base_loopbreak(_eventBase);
		if (_eventLoopThread.joinable()) {
			_eventLoopThread.join();
		}
	}

	void EventLoop::eventLoopThreadEntry(void) {
		auto result = event_base_loop(EventLoop::mainLoop()->eventBase(), EVLOOP_NO_EXIT_ON_EMPTY);
		if (result != 0) {
			perror("Event loop exited!");
		}
	}

} /* namespace Jupiter */
