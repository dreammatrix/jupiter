/*
 * OperationQueue.inl
 *
 *  Created on: Feb 7, 2015
 *      Author: unalone
 */
namespace Jupiter {
	namespace Utilities {

		template <typename _OperationType>
		void OperationQueue::enqueue(const _OperationType operation) const {
			dispatch_async(_queue, ^{ operation(); });
		}

		template <typename _OperationType>
		void OperationQueue::enqueue(const _OperationType operation, float time) const {
			if (time != 0) {
				dispatch_time_t dtime = dispatch_time(0, (int64_t)(time * 1e9));
				dispatch_after(dtime, _queue, ^{ operation(); });
			} else {
				dispatch_async(_queue, ^{ operation(); });
			}
		}

		template <typename _OperationType>
		void OperationQueue::call(const _OperationType operation) const {
			long queueId = ( long )dispatch_get_context(_queue);
			if (queueId == _queueId) {
				operation();
			} else {
				dispatch_sync(_queue, ^{ operation(); });
			}
		}

	} /* namespace Utilities */
} /* namespace Jupiter */
