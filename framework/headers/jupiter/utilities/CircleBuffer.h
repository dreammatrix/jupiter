/*
 * CircleBuffer.h
 *
 *  Created on: Feb 14, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_JUPITER_SOURCE_NETWORK_CIRCLEBUFFER_H_
#define FRAMEWORKS_JUPITER_SOURCE_NETWORK_CIRCLEBUFFER_H_

#include <memory>

namespace Jupiter {
	namespace Utilities {

		class CircleBuffer {
		public:
			CircleBuffer (unsigned long size);
			virtual ~CircleBuffer ();

			bool empty (void) const;
			bool full (void) const;

			unsigned long count (void) const;
			unsigned long space (void) const;

			unsigned long push (char *buffer, unsigned long count);
			void erase (unsigned long count);

			void read (unsigned long count, char *buffer, unsigned long begin = 0);

		private:
			char *_buffer = nullptr;
			unsigned long _head = 0;
			unsigned long _tail = 0;
			unsigned long _size = 0;
		};

		typedef std::shared_ptr<CircleBuffer> CircleBufferRef;

	} /* namespace Network */
} /* namespace Jupiter */

#endif /* FRAMEWORKS_JUPITER_SOURCE_NETWORK_CIRCLEBUFFER_H_ */
