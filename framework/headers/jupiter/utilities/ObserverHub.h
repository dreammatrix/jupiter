/*
 * ObserverHub.h
 *
 *  Created on: Sep 2, 2014
 *      Author: unalone
 */

#ifndef OBSERVERHUB_H_
#define OBSERVERHUB_H_

#include "jupiter/Types.h"
#include <jupiter/pool/memory_pool.h>
#include <jupiter/utilities/OperationQueue.h>
#include <mutex>
#include <vector>

namespace Jupiter {
	namespace Utilities {

		template <typename... Args>
		class ObserverHub {
		public:
			ObserverHub() = default;
			ObserverHub(dispatch_queue_t queue)
				: _queue(queue) {
			}

			ObserverHub(const OperationQueue &queue)
				: _queue(queue) {
			}

			intptr_t addObserver(const Action<const Action<> &, Args...> &observer) {
				auto ptr = MemPoolAllocater<Action<const std::function<void()> &, Args...>>::construct(observer);
				std::lock_guard<decltype(_mutex)> lock(_mutex);
				_observers.push_back(ptr);
				return (intptr_t)(ptr.get());
			}

			void removeObserver(intptr_t observerId) {
				std::lock_guard<decltype(_mutex)> lock(_mutex);
				auto it = std::find_if(_observers.begin(), _observers.end(),
									   [&](const std::shared_ptr<Action<const std::function<void()> &, Args...>> &ptr) { return ( intptr_t )ptr.get() == observerId; });

				if (it == _observers.end()) {
					return;
				}

				_observers.erase(it);
			}

			void fire(const Action<> &completion, Args &&... params) {
				std::lock_guard<decltype(_mutex)> lock(_mutex);
				if (_observers.size() > 0) {
					int finishCount = 0;
					int totalCount = _observers.size();

					for (auto &observer : _observers) {
						_queue.enqueue([=]() {
							(*observer)(
								[=]() mutable {
									++finishCount;
									if (finishCount >= totalCount) {
										if (completion != nullptr) {
											completion();
										}
									}
								},
								std::forward<Args>(params)...);
						});
					}
				} else {
					if (completion != nullptr) {
						completion();
					}
				}
			}

		private:
			OperationQueue _queue;
			std::vector<std::shared_ptr<Action<const Action<> &, Args...>>> _observers;

			std::recursive_mutex _mutex;
		};

		template <typename... Args>
		using ObserverHubRef = std::shared_ptr<Utilities::ObserverHub<const Args &...>>;
	} /* namespace Utilities */
} /* namespace Jupiter */

#endif /* OBSERVERHUB_H_ */
