/*
 * Utilities.h
 *
 *  Created on: Mar 4, 2013
 *      Author: unalone
 */

#ifndef JUPITER_UTILITIES_CRYPTO_H_
#define JUPITER_UTILITIES_CRYPTO_H_

#include <openssl/aes.h>
#include <string>

namespace Jupiter {
	namespace Utilities {
		class Crypto {
		public:
			static void initialize(void);
			static const std::string &&sha256Hash(const std::string &buffer);
			static const std::string &&aesCrypto(const std::string &buffer, const std::string &key);
			static const std::string &&aesDecrypto(const std::string &buffer, const std::string &key);

		private:
			static unsigned char _iv[ AES_BLOCK_SIZE ];
		};
	}  // namespace Utilities
}  // namespace Jupiter

#endif /* JUPITER_UTILITIES_CRYPTO_H_ */
