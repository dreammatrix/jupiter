/*
 * Channel.h
 *
 *  Created on: Sep 12, 2014
 *      Author: unalone
 */

#ifndef CHANNEL_H_
#define CHANNEL_H_

#include <boost/any.hpp>
#include <cstdint>
#include <jupiter/core/PropertyContainer.h>
#include <jupiter/network/RpcInvoker.h>
#include <jupiter/network/Types.h>
#include <jupiter/pool/ProtocolPool.h>
#include <jupiter/utilities/ObserverHub.h>
#include <variant>

namespace Jupiter {
	namespace Network {
		struct IMessageConfig {
			virtual ~IMessageConfig() = default;
			virtual const std::variant<int32_t, std::string> &id(void) = 0;
			virtual MessageRef createIncomingMessageInstance(void) = 0;
			virtual MessageRef createReturnMessageInstance(void) = 0;
			virtual const std::variant<MessageHandlerType, RpcMethodType> &handler(void) = 0;
		};

		template <typename MessageType, typename ReturnMessageType>
		class MessageConfigForService : public IMessageConfig {
		public:
			MessageConfigForService(const std::variant<int32_t, std::string> &id, const std::variant<MessageHandlerType, RpcMethodType> &handler)
				: _id(id)
				, _handler(handler) {
			}

			virtual const std::variant<int32_t, std::string> &id(void) {
				return _id;
			}
			virtual MessageRef createIncomingMessageInstance(void) {
				return ProtocolPool<MessageType>::construct();
			}
			virtual MessageRef createReturnMessageInstance(void) {
				return ProtocolPool<ReturnMessageType>::construct();
			}
			virtual const std::variant<MessageHandlerType, RpcMethodType> &handler(void) {
				return _handler;
			}

		private:
			std::variant<int32_t, std::string> _id;
			std::variant<MessageHandlerType, RpcMethodType> _handler;
		};

		template <typename ReturnMessageType>
		class MessageConfigForCaller : public IMessageConfig {
		public:
			MessageConfigForCaller(const std::variant<int32_t, std::string> &id)
				: _id(id) {
			}

			virtual const std::variant<int32_t, std::string> &id(void) {
				return _id;
			}
			virtual MessageRef createIncomingMessageInstance(void) {
				return MessageRef();
			}
			virtual MessageRef createReturnMessageInstance(void) {
				return ProtocolPool<ReturnMessageType>::construct();
			}
			virtual const std::variant<MessageHandlerType, RpcMethodType> &handler(void) {
				return _handler;
			}

		private:
			std::variant<int32_t, std::string> _id;
			std::variant<MessageHandlerType, RpcMethodType> _handler;
		};

		class MessageConfigForEmptyMessage : public IMessageConfig {
		public:
			MessageConfigForEmptyMessage(const std::variant<int32_t, std::string> &id, const std::variant<MessageHandlerType, RpcMethodType> &handler)
				: _id(id)
				, _handler(handler) {
			}

			virtual const std::variant<int32_t, std::string> &id(void) {
				return _id;
			}
			virtual MessageRef createIncomingMessageInstance(void) {
				return MessageRef();
			}
			virtual MessageRef createReturnMessageInstance(void) {
				return MessageRef();
			}
			virtual const std::variant<MessageHandlerType, RpcMethodType> &handler(void) {
				return _handler;
			}

		private:
			std::variant<int32_t, std::string> _id;
			std::variant<MessageHandlerType, RpcMethodType> _handler;
		};

		using MessageConfigPtr = std::shared_ptr<IMessageConfig>;

		template <typename MessageType, typename ReturnMessageType = MessageType>
		std::shared_ptr<IMessageConfig> createMessageConfig(const std::variant<int32_t, std::string> &id, const std::variant<MessageHandlerType, RpcMethodType> &handler) {
			return MemPoolAllocater<MessageConfigForService<MessageType, ReturnMessageType>>::construct(id, handler);
		}

		template <typename ReturnMessageType>
		std::shared_ptr<IMessageConfig> createMessageConfig(const std::variant<int32_t, std::string> &id) {
			return MemPoolAllocater<MessageConfigForCaller<ReturnMessageType>>::construct(id);
		}

		inline std::shared_ptr<IMessageConfig> createEmptyMessageConfig(const std::variant<int32_t, std::string> &id, const std::variant<MessageHandlerType, RpcMethodType> &handler) {
			return MemPoolAllocater<MessageConfigForEmptyMessage>::construct(id, handler);
		}

		struct IChannel : public IRpcInvoker, public IPropertyContainer {
			virtual ~IChannel() = default;

			virtual uint16_t channelId() const = 0;
			virtual const ConnectionRef &connection() const = 0;

			virtual const std::tuple<std::string, short> &address(void) const = 0;

			// 设置替代地址，当设置了替代地址后，远程对应的channel会认为这条channel链接来自于替代地址而不是原本的地址
			virtual const std::tuple<std::string, short> &alternativeAddress(void) const = 0;

			virtual void setAlternativeAddress(const std::tuple<std::string, short> &value) = 0;

			virtual void setMessageConfig(const std::vector<MessageConfigPtr> &configs) = 0;

			virtual const std::shared_ptr<Utilities::ObserverHub<const ChannelRef &>> &closed() const = 0;
			virtual const std::shared_ptr<Utilities::ObserverHub<const ChannelRef &, const NetworkPacketRef &>> &dataArrived() const = 0;

			virtual void close(const std::function<void()> &completion) = 0;

			virtual void send(int messageId) = 0;
			virtual void send(int messageId, const MessageRef &message) = 0;

			virtual void send(const Network::NetworkPacketRef &packet) = 0;
		};

	}  // namespace Network
}  // namespace Jupiter

#endif /* CHANNEL_H_ */
