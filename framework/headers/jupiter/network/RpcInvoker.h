/*
 * RpcInvoker.h
 *
 *  Created on: Sep 14, 2014
 *      Author: unalone
 */

#ifndef RPC_INVOKER_HEADER_H_
#define RPC_INVOKER_HEADER_H_

#include <jupiter/network/Types.h>
#include <jupiter/utilities/ObserverHub.h>

namespace Jupiter {
	namespace Network {

		using RpcMethodType = Action<const ChannelRef &, const std::string &, const MessageRef &, const Action<RpcErrorCode, const MessageRef &> &>;

		struct IRpcInvoker {
			virtual ~IRpcInvoker() = default;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 */
			virtual void invoke(const std::string &methodName) = 0;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param parameter 方法参数对象。
			 */
			virtual void invoke(const std::string &methodName, const MessageRef &parameter) = 0;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param completion 用于接收调用返回结果的处理器，第一个参数是错误代码，指出调用是否成功；第二个参数是方法返回值对象。
			 */
			virtual void invoke(const std::string &methodName, const Action<RpcErrorCode, const MessageRef &> &completion) = 0;

			/**
			 * 发起一个rpc调用，所有的调用都是异步的，也就是说，不管成功与否，调用都会立即返回。
			 * @param methodName 要调用的方法名称。
			 * @param parameter 方法参数对象。
			 * @param completion 用于接收调用返回结果的处理器，第一个参数是错误代码，指出调用是否成功；第二个参数是方法返回值对象。
			 */
			virtual void invoke(const std::string &methodName, const MessageRef &parameter, const Action<RpcErrorCode, const MessageRef &> &completion) = 0;
		};

	}  // namespace Network
}  // namespace Jupiter

#endif /* RPC_INVOKER_HEADER_H_ */
