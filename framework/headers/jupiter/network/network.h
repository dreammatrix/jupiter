/*
 * network.h
 *
 *  Created on: Sep 13, 2014
 *      Author: unalone
 */

#ifndef NETWORK_H_
#define NETWORK_H_

#include <jupiter/network/Channel.h>
#include <jupiter/network/Connection.h>
#include <jupiter/network/Exception.h>
#include <jupiter/network/Types.h>

namespace Jupiter {
	namespace Network {

		ConnectionRef connectionForListen (const std::string &adapterName, ushort port, const Action<const Action<> &, const ConnectionRef&> &connectionArrivalHandler);
		ConnectionRef connectionForListen (const std::string &path, const Action<const Action<> &, const ConnectionRef&> &connectionArrivalHandler);
		ConnectionRef connectToServer (const std::string &address, ushort port, const Action<bool> &completion);
		ConnectionRef connectToServer (const std::string &path, const Action<bool> &completion);

	}  // namespace Network
}  // namespace Jupiter

#endif /* NETWORK_H_ */
