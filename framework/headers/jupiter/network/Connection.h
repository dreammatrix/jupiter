/*
 * Connection.h
 *
 *  Created on: Sep 12, 2014
 *      Author: unalone
 */

#ifndef CONNECTION_H_
#define CONNECTION_H_

#include "jupiter/Types.h"
#include "jupiter/utilities/ObserverHub.h"
#include <cstdint>
#include <jupiter/core/core.h>
#include <jupiter/network/Types.h>
#include <optional>
#include <tuple>

namespace Jupiter {
	namespace Network {

		struct IConnection : public IPropertyContainer {
			virtual ~IConnection() = default;

			virtual uint64_t id(void) const = 0;

			virtual const std::tuple<std::string, short> &address(void) const = 0;

			virtual const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> connectionArrived(void) const = 0;
			virtual const std::shared_ptr<Utilities::ObserverHub<const ConnectionRef &>> connectionClosed(void) const = 0;

			virtual void setPreDispatcher(const Method<std::tuple<ConnectionRef, int>, const ConnectionRef &, int> &processor) = 0;
			virtual void setChannelInitializer(const Action<const ConnectionRef &, const ChannelRef &> &handler) = 0;

			virtual ChannelRef channelOfId(int channelId) const = 0;

			virtual ChannelRef fork() = 0;

			virtual void close(void) = 0;
		};

	}  // namespace Network
}  // namespace Jupiter

#endif /* CONNECTION_H_ */
