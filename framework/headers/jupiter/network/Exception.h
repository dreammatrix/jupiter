/*
 * NetworkException.h
 *
 *  Created on: Sep 12, 2014
 *      Author: unalone
 */


#ifndef NETWORK_EXCEPTION_H_
#define NETWORK_EXCEPTION_H_

#include <boost/any.hpp>
#include <exception>
#include <jupiter/core/PropertyContainer.h>
#include <jupiter/network/RpcInvoker.h>
#include <jupiter/network/Types.h>
#include <jupiter/pool/ProtocolPool.h>
#include <jupiter/utilities/ObserverHub.h>
#include <variant>

namespace Jupiter {
	namespace Network {
		struct NetworkException {
			virtual ~NetworkException() {}
		};

		struct ChannelStatusException : public NetworkException {};

		struct ConnectionStatusException : public NetworkException {};
	}  // namespace Network
}  // namespace Jupiter

#endif	// NETWORK_EXCEPTION_H_