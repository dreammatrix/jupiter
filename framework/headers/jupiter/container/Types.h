/*
 * Types.h
 *
 *  Created on: Apr 7, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_PLUGINS_TYPES_H_
#define FRAMEWORKS_HEADERS_JUPITER_PLUGINS_TYPES_H_

#include <jupiter/Types.h>

namespace Jupiter {

	class Service;
	struct IServiceContainer;

	using ServiceRef = std::shared_ptr<Service>;
	using ServiceContainerRef = std::shared_ptr<IServiceContainer>;

}  // namespace Jupiter

#endif /* FRAMEWORKS_HEADERS_JUPITER_PLUGINS_TYPES_H_ */
