/*
 * ResourceManager.h
 *
 *  Created on: Mar 24, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_CORE_RESOURCEMANAGER_H_
#define FRAMEWORKS_HEADERS_JUPITER_CORE_RESOURCEMANAGER_H_

#include <jupiter/core/Types.h>

namespace Jupiter {

	class ResourceManager {
	public:
		static ResourceManager *sharedInstance ();

		bool loadConfig(const std::string &fileName, boost::property_tree::ptree &root);
	private:
		static ResourceManager *_sharedInstance;

		ResourceManager () = default;
	};

} /* namespace Jupiter */

#endif /* FRAMEWORKS_HEADERS_JUPITER_CORE_RESOURCEMANAGER_H_ */
