/*
 * IPlugin.h
 *
 *  Created on: Apr 6, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_CORE_IPLUGINFACTORY_H_
#define FRAMEWORKS_HEADERS_JUPITER_CORE_IPLUGINFACTORY_H_

#include <jupiter/core/Types.h>

namespace Jupiter {

	struct IPluginFactory {
		virtual ~IPluginFactory(void) = default;

		virtual void *createInstance () = 0;
	};

}  // namespace Jupiter


#endif /* FRAMEWORKS_HEADERS_JUPITER_CORE_IPLUGINFACTORY_H_ */
