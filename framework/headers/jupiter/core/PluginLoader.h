/*
 * PluginLoader.h
 *
 *  Created on: Apr 6, 2015
 *      Author: unalone
 */

#ifndef FRAMEWORKS_HEADERS_JUPITER_CORE_PLUGINLOADER_H_
#define FRAMEWORKS_HEADERS_JUPITER_CORE_PLUGINLOADER_H_

#include <jupiter/core/IPluginFactory.h>
#include <jupiter/core/Types.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/types.h>
#include <unistd.h>
#include <dlfcn.h>

namespace Jupiter {

	template<typename IPluginType>
	class PluginLoader {
	public:
		PluginLoader (const std::string &libPath, const std::string factoryEntryName = "createPluginFactory");
		~PluginLoader ();

		bool initialize ();

		const std::shared_ptr<IPluginType> &&createInstance ();
	private:
		typedef void *(*PLUGIN_ENTRY_FUNC_TYPE)(void);
		std::string _libPath;
		std::string _pluginFactoryEntryName;
		PLUGIN_ENTRY_FUNC_TYPE _pluginCreatorFunc;
		void *_handle = nullptr;
	};

	template<typename IPluginType>
	using PluginLoaderRef = std::shared_ptr<PluginLoader<IPluginType>>;

} /* namespace Jupiter */


#include <jupiter/core/PluginLoader.inl>

#endif /* FRAMEWORKS_HEADERS_JUPITER_CORE_PLUGINLOADER_H_ */
