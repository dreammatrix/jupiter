if(NOT DEFINED ENV{HOME})
    # 没有找到HOME环境变量
    message(FATAL_ERROR "not defined environment variable:HOME")  
endif()

find_path(
  LIBJUPITER_INCLUDE_DIR
  NAMES jupiter/jupiter.h jupiter/Jupiter.h
  /usr/include
  /usr/local/include
  $ENV{HOME}/opt/include
  )

find_library(
  LIBJUPITER_LIBRARIES jupiter
  paths ${CMAKE_CURRENT_BINARY_DIR}/lib $ENV{HOME}/opt/lib/ /usr/lib/ /usr/local/lib/
)

mark_as_advanced(
	LIBJUPITER_INCLUDE_DIR
	LIBJUPITER_LIBRARIES
)