if(NOT DEFINED ENV{HOME})
    # 没有找到HOME环境变量
    message(FATAL_ERROR "not defined environment variable:HOME")  
endif()

find_path(
  LIBJUPITER_ACCOUNT_SERVICE_PROTOCOLS_INCLUDE_DIR
  NAMES jupiter/protocols/services/account/accountservice.pb.h jupiter/protocols/services/account/AccountService.pb.h 
  /usr/include
  /usr/local/include
  $ENV{HOME}/opt/include
  )

find_library(
  LIBJUPITER_ACCOUNT_SERVICE_PROTOCOLS_LIBRARIES jupiter.protocols.services.account
  paths $ENV{HOME}/opt/lib/ /usr/lib/ /usr/local/lib/
)

mark_as_advanced(
	LIBJUPITER_ACCOUNT_SERVICE_PROTOCOLS_INCLUDE_DIR
	LIBJUPITER_ACCOUNT_SERVICE_PROTOCOLS_LIBRARIES
)