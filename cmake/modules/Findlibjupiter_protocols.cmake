if(NOT DEFINED ENV{HOME})
    # 没有找到HOME环境变量
    message(FATAL_ERROR "not defined environment variable:HOME")  
endif()

find_path(
  LIBJUPITER_PROTOCOLS_INCLUDE_DIR
  NAMES jupiter/protocols/jupiterapi.pb.h jupiter/protocols/JupiterApi.pb.h
  /usr/include
  /usr/local/include
  $ENV{HOME}/opt/include
  )

find_library(
  LIBJUPITER_PROTOCOLS_LIBRARIES jupiter_protocols
  paths $ENV{HOME}/opt/lib/ /usr/lib/ /usr/local/lib/
)

mark_as_advanced(
	LIBJUPITER_PROTOCOLS_INCLUDE_DIR
	LIBJUPITER_PROTOCOLS_LIBRARIES
)